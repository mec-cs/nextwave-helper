# NextWave Helper
The application will be able to operate in 2 distinct modes:
- Normal mode 
- Disaster mode

## Normal Mode
The application will have the following features: 
- Help Requests
- Donation Centers
- Missing person locator
- Relief center locations
- Announcements

## Disaster mode
The application will have the following features:
- Basic text communication
- Emergency help request
- Geo-location sharing