package meccsb.project.nextwavehelper.announcements;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;

public class AnnouncementAdapter extends ListAdapter<Announcement, AnnouncementAdapter.AnnouncementHolder> {
    AnnouncementAdapter() {
        super(DIFF_CALLBACK);
    }
    private static final DiffUtil.ItemCallback<Announcement> DIFF_CALLBACK = new DiffUtil.ItemCallback<Announcement>() {
        @Override
        public boolean areItemsTheSame(@NonNull Announcement oldItem, @NonNull Announcement newItem) {
            return oldItem.getAnnouncementID() == newItem.getAnnouncementID();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Announcement oldItem, @NonNull Announcement newItem) {
            return oldItem.getDescription().equals(newItem.getDescription()) &&
                    oldItem.getImportanceLevel() == newItem.getImportanceLevel() &&
                    oldItem.getTime().equals(newItem.getTime());
        }
    };
    @NonNull
    @Override
    public AnnouncementHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.announcement_item, parent, false);
        return new AnnouncementHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull AnnouncementHolder holder, int position) {
        Announcement currentAnnouncement = getItem(position);
        holder.timeText.setText(currentAnnouncement.getTime());
        holder.descriptionText.setText(Html.fromHtml(currentAnnouncement.getDescription(),Html.FROM_HTML_MODE_LEGACY));
        switch (currentAnnouncement.getImportanceLevel()){
            case 0: // Low - Green
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_maps);
                break;
            case 1: // Medium - Orange
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_help_requests);
                break;
            case 2: // High - Red
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_disaster_mode);
                break;
        }
    }

    class AnnouncementHolder extends RecyclerView.ViewHolder{
        private final RelativeLayout relativeLayout;
        private final TextView timeText;
        private final TextView descriptionText;

        AnnouncementHolder(@NonNull View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.announcement_item_layout);
            timeText = itemView.findViewById(R.id.announcement_item_time);
            descriptionText = itemView.findViewById(R.id.announcement_item_description);
        }
    }
}
