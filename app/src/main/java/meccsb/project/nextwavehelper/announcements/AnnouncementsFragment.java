package meccsb.project.nextwavehelper.announcements;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.material.navigation.NavigationView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;
import meccsb.project.nextwavehelper.roomdb.viewmodels.AnnouncementViewModel;

public class AnnouncementsFragment extends Fragment {
    /*
     * Step 1: Here, we need to inject the ViewModelFactory.
     * */
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private final AnnouncementAdapter adapter = new AnnouncementAdapter();
    private AnnouncementViewModel announcementViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        announcementViewModel = ViewModelProviders.of(this, viewModelFactory).get(AnnouncementViewModel.class);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_announcements, container, false);
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);

        getActivity().setTitle(R.string.title_announcements);
        final RecyclerView recyclerView = view.findViewById(R.id.announcement_recycler_view);
        final ProgressBar progressBar = view.findViewById(R.id.announcement_progressBar);
        final LinearLayout mainContent = view.findViewById(R.id.announcements_main_content);

        navigationView.setCheckedItem(R.id.nav_announcements);

        announcementViewModel.getAllAnnouncements().observe(this, new Observer<List<Announcement>>() {
            @Override
            public void onChanged(List<Announcement> announcements) {
                adapter.submitList(announcements);
                if(announcements.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    mainContent.setVisibility(View.VISIBLE);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        // So that announcements are refreshed every time user visits this fragment
        announcementViewModel.refreshFromWeb();
    }

    private void configureDagger(){
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }

}
