package meccsb.project.nextwavehelper.api;

import java.util.List;

import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;
import retrofit2.Call;
import retrofit2.http.GET;

public interface AnnouncementWebservice {
    @GET("announcement")
    Call<List<Announcement>> getAnnouncements();
}