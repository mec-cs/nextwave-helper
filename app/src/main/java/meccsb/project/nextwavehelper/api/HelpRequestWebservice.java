package meccsb.project.nextwavehelper.api;

import java.util.List;

import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface HelpRequestWebservice {
    @GET("help_request")
    Call<List<HelpRequest>> getHelpRequests();

    @POST("help_request")
    Call<HelpRequest> addHelpRequest(@Body HelpRequest helpRequest);

    @POST("help_request/complete")
    Call<Void> markAsCompleted(@Body HelpRequest helpRequest);
}