package meccsb.project.nextwavehelper.api;

import java.util.List;

import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface MissingPersonWebservice {
    @GET("missing_person")
    Call<List<MissingPerson>> getMissingPersons();

    @POST("missing_person")
    Call<MissingPerson> addMissingPerson(@Body MissingPerson missingPerson);
}