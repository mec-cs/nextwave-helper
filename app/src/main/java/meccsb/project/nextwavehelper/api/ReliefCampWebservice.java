package meccsb.project.nextwavehelper.api;

import java.util.List;

import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ReliefCampWebservice {
    @GET("relief_camp_location")
    Call<List<ReliefCamp>> getReliefCamps();
}
