package meccsb.project.nextwavehelper.api;

import com.google.gson.annotations.Expose;

public class RescueWorker {
    @Expose
    private String WorkerName;

    @Expose
    private String WorkerPhone;

    @Expose
    private Double Latitude;

    @Expose
    private Double Longitude;

    public RescueWorker(String workerName, String workerPhone) {
        WorkerName = workerName;
        WorkerPhone = workerPhone;
        Latitude = 0d;
        Longitude = 0d;
    }

    public String getWorkerName() {
        return WorkerName;
    }

    public void setWorkerName(String workerName) {
        WorkerName = workerName;
    }

    public String getWorkerPhone() {
        return WorkerPhone;
    }

    public void setWorkerPhone(String workerPhone) {
        WorkerPhone = workerPhone;
    }

    public Double getLatitude() {
        return Latitude;
    }

    public void setLatitude(Double latitude) {
        Latitude = latitude;
    }

    public Double getLongitude() {
        return Longitude;
    }

    public void setLongitude(Double longitude) {
        Longitude = longitude;
    }
}
