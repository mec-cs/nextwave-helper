package meccsb.project.nextwavehelper.api;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface RescueWorkerWebservice {
    @POST("rescue_worker")
    Call<Void> sendUpdate(@Body RescueWorker rescueWorker);

    @POST("rescue_worker/delete/{phone}")
    Call<Void> deleteWorker(@Path("phone")String phoneNumber);
}
