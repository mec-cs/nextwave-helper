package meccsb.project.nextwavehelper.dagger.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import meccsb.project.nextwavehelper.main.MainActivity;

/*
 * We modify our ActivityModule by adding the
 * FragmentModule to the Activity which contains
 * the fragment.
 */

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract MainActivity contributeMainActivity();
}
