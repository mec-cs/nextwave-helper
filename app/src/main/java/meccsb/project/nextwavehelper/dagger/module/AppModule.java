package meccsb.project.nextwavehelper.dagger.module;

import android.app.Application;

import androidx.room.Room;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import meccsb.project.nextwavehelper.BuildConfig;
import meccsb.project.nextwavehelper.api.AnnouncementWebservice;
import meccsb.project.nextwavehelper.api.HelpRequestWebservice;
import meccsb.project.nextwavehelper.api.MissingPersonWebservice;
import meccsb.project.nextwavehelper.api.ReliefCampWebservice;
import meccsb.project.nextwavehelper.api.RescueWorkerWebservice;
import meccsb.project.nextwavehelper.main.LocationListener;
import meccsb.project.nextwavehelper.roomdb.database.MyDatabase;
import meccsb.project.nextwavehelper.roomdb.database.dao.AnnouncementDao;
import meccsb.project.nextwavehelper.roomdb.database.dao.HelpRequestDao;
import meccsb.project.nextwavehelper.roomdb.database.dao.MissingPersonDao;
import meccsb.project.nextwavehelper.roomdb.database.dao.ReliefCampDao;
import meccsb.project.nextwavehelper.roomdb.repositories.AnnouncementRepository;
import meccsb.project.nextwavehelper.roomdb.repositories.HelpRequestRepository;
import meccsb.project.nextwavehelper.roomdb.repositories.MissingPersonRepository;
import meccsb.project.nextwavehelper.roomdb.repositories.ReliefCampRepository;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
public class AppModule {

    /*
     * The method returns the MyDatabase object
     * */
    @Provides
    @Singleton
    MyDatabase provideDatabase(Application application) {
        return  Room.databaseBuilder(application,
                MyDatabase.class, "NextWaveHelper.db")
                .fallbackToDestructiveMigration()
                .build();
    }
    @Provides
    Executor provideExecutor() {
        return Executors.newSingleThreadExecutor();
    }
    /*
     * We need the AnnouncementDao module.
     * For this, We need the MyDatabase object
     * So we will define the providers for this here in this module.
     * */

    @Provides
    @Singleton
    AnnouncementDao provideAnnouncementDao(MyDatabase myDatabase) { return myDatabase.announcementDao(); }

    @Provides
    @Singleton
    AnnouncementRepository provideAnnouncementRepository
            (AnnouncementWebservice webservice, AnnouncementDao announcementDao, Executor executor) {
        return new AnnouncementRepository(webservice, announcementDao, executor);
    }

    @Provides
    @Singleton
    MissingPersonDao provideMissingPersonDao(MyDatabase myDatabase) {
        return myDatabase.missingPersonDao();
    }

    @Provides
    @Singleton
    MissingPersonRepository provideMissingPersonRepository
            (MissingPersonWebservice webservice, MissingPersonDao missingPersonDao, Executor executor) {
        return new MissingPersonRepository(webservice, missingPersonDao, executor);
    }

    @Provides
    @Singleton
    HelpRequestDao provideHelpRequestDao(MyDatabase myDatabase) { return myDatabase.helpRequestDao(); }

    @Provides
    @Singleton
    HelpRequestRepository provideHelpRequestRepository
            (HelpRequestWebservice webservice, HelpRequestDao helpRequestDao, Executor executor) {
        return new HelpRequestRepository(webservice, helpRequestDao, executor);
    }


    @Provides
    @Singleton
    ReliefCampDao provideReliefCampDao(MyDatabase myDatabase) { return myDatabase.reliefCampDao(); }

    @Provides
    @Singleton
    ReliefCampRepository provideReliefCampRepository
            (ReliefCampWebservice webservice, ReliefCampDao reliefCampDao, Executor executor) {
        return new ReliefCampRepository(webservice, reliefCampDao, executor);
    }

    @Provides
    @Singleton
    LocationListener provideLocationListener(Application application){
        return new LocationListener(application);
    }

    @Provides
    Gson provideGson() { return new GsonBuilder().create(); }

    @Provides
    Retrofit provideRetrofit(Gson gson) {
        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(chain -> {
                    Request originalRequest = chain.request();
                    Request newRequest = originalRequest.newBuilder()
                            .header("api-key", BuildConfig.WebAPIKey)
                            .build();
                    return chain.proceed(newRequest);
                }).build();
        String BASE_URL = "https://www.nextwavehelper.cf/api/";
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    AnnouncementWebservice provideAnnouncementWebservice(Retrofit restAdapter) {
        return restAdapter.create(AnnouncementWebservice.class);
    }

    @Provides
    @Singleton
    HelpRequestWebservice provideHelpRequestWebservice(Retrofit restAdapter) {
        return restAdapter.create(HelpRequestWebservice.class);
    }

    @Provides
    @Singleton
    MissingPersonWebservice provideMissingPersonWebservice(Retrofit restAdapter) {
        return restAdapter.create(MissingPersonWebservice.class);
    }

    @Provides
    @Singleton
    ReliefCampWebservice provideReliefCampWebservice(Retrofit restAdapter) {
        return restAdapter.create(ReliefCampWebservice.class);
    }

    @Provides
    @Singleton
    RescueWorkerWebservice provideRescueWorkerWebservice(Retrofit restAdapter) {
        return restAdapter.create(RescueWorkerWebservice.class);
    }
}

