package meccsb.project.nextwavehelper.dagger.module;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import meccsb.project.nextwavehelper.announcements.AnnouncementsFragment;
import meccsb.project.nextwavehelper.disastermode.DisasterFragmentChat;
import meccsb.project.nextwavehelper.disastermode.DisasterFragmentFind;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsAddFragment;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsAddPlacePickerFragment;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsListAssFragment;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsListFragment;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsListRegFragment;
import meccsb.project.nextwavehelper.main.HomeFragment;
import meccsb.project.nextwavehelper.main.SettingsFragment;
import meccsb.project.nextwavehelper.maps.AffectedLocationsFragment;
import meccsb.project.nextwavehelper.maps.ReliefCampListFragment;
import meccsb.project.nextwavehelper.maps.ReliefCampListMapFragment;
import meccsb.project.nextwavehelper.missingpersons.MissingPersonAddFragment;
import meccsb.project.nextwavehelper.missingpersons.MissingPersonsFragment;

/*
 * We define the name of the Fragment we are going
 * to inject the ViewModelFactory into. i.e. in our case
 * The name of the fragment: UserProfileFragment
 */

@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract HomeFragment contributeHomeFragment();

    @ContributesAndroidInjector
    abstract AnnouncementsFragment contributeAnnouncementsFragment();

    @ContributesAndroidInjector
    abstract HelpRequestsListFragment contributeHelpRequestsFragment();

    @ContributesAndroidInjector
    abstract HelpRequestsListRegFragment contributeHelpRequestsRegFragment();

    @ContributesAndroidInjector
    abstract HelpRequestsListAssFragment contributeHelpRequestsAssFragment();

    @ContributesAndroidInjector
    abstract HelpRequestsAddFragment contributeHelpRequestsAddFragment();

    @ContributesAndroidInjector
    abstract HelpRequestsAddPlacePickerFragment contributeHelpRequestsAddPlacePickerFragment();

    @ContributesAndroidInjector
    abstract MissingPersonsFragment contributeMissingPersonsFragment();

    @ContributesAndroidInjector
    abstract MissingPersonAddFragment contributeMissingPersonAddFragment();

    @ContributesAndroidInjector
    abstract ReliefCampListFragment contributeReliefCampsListFragment();

    @ContributesAndroidInjector
    abstract ReliefCampListMapFragment contributeReliefCampsListMapFragment();

    @ContributesAndroidInjector
    abstract AffectedLocationsFragment contributeAffectedLocationsFragment();

    @ContributesAndroidInjector
    abstract DisasterFragmentChat contributeDisasterFragmentChat();

    @ContributesAndroidInjector
    abstract DisasterFragmentFind contributeDisasterFragmentFind();

    @ContributesAndroidInjector
    abstract SettingsFragment contributeSettingsFragment();
}