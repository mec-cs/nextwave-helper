package meccsb.project.nextwavehelper.dagger.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import meccsb.project.nextwavehelper.dagger.ViewModelKey;
import meccsb.project.nextwavehelper.roomdb.viewmodels.AnnouncementViewModel;
import meccsb.project.nextwavehelper.roomdb.viewmodels.FactoryViewModel;
import meccsb.project.nextwavehelper.roomdb.viewmodels.HelpRequestViewModel;
import meccsb.project.nextwavehelper.roomdb.viewmodels.MissingPersonViewModel;
import meccsb.project.nextwavehelper.roomdb.viewmodels.ReliefCampViewModel;

@Module
public abstract class ViewModelModule {
    /*
     * This method basically says
     * inject this object into a Map using the @IntoMap annotation,
     * with the  UserProfileViewModel.class as key,
     * and a Provider(binder?) that will build a UserProfileViewModel
     * object.
     * */

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(FactoryViewModel factory);

    @Binds
    @IntoMap
    @ViewModelKey(AnnouncementViewModel.class)
    abstract ViewModel bindAnnouncementViewModel(AnnouncementViewModel announcementViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(HelpRequestViewModel.class)
    abstract ViewModel bindHelpRequestViewModel(HelpRequestViewModel helpRequestViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MissingPersonViewModel.class)
    abstract ViewModel bindMissingPersonViewModel(MissingPersonViewModel missingPersonViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(ReliefCampViewModel.class)
    abstract ViewModel bindReliefCampViewModel(ReliefCampViewModel reliefCampViewModel);
}
