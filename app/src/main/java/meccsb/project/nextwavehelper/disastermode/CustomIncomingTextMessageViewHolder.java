package meccsb.project.nextwavehelper.disastermode;

import android.view.View;
import android.widget.TextView;

import com.stfalcon.chatkit.messages.MessageHolders;

import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.disastermode.entities.Message;

public class CustomIncomingTextMessageViewHolder extends MessageHolders.IncomingTextMessageViewHolder<Message> {

    private TextView authorTextView;

    public CustomIncomingTextMessageViewHolder(View itemView, Object payload) {
        super(itemView, payload);
        authorTextView = itemView.findViewById(R.id.messageAuthor);
    }

    @Override
    public void onBind(Message message) {
        super.onBind(message);
        authorTextView.setText(String.format("- %s", message.getUser().getName()));
    }
}
