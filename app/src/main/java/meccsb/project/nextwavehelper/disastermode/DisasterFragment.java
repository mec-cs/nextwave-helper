package meccsb.project.nextwavehelper.disastermode;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Fade;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import java.util.Objects;

import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;

public class DisasterFragment extends Fragment {
    private static final String TAG_FRAGMENT_PEERS = "fragment_peers";
    private static final String TAG_FRAGMENT_CHAT = "fragment_chat";
    private static final String TAG_FRAGMENT_FIND = "fragment_find";

    private FragmentManager fragmentManager;
    private Fragment currentFragment;
    private static BottomNavigationView bottomNavigationView;

    private Fragment disasterPeersList = DisasterFragmentPeersList.newInstance();
    private static DisasterFragmentChat disasterFragmentChat;
    private static DisasterFragmentFind disasterFragmentFind;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disaster, container, false);
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);

        hideFragment(TAG_FRAGMENT_PEERS);
        hideFragment(TAG_FRAGMENT_FIND);
        hideFragment(TAG_FRAGMENT_CHAT);

        bottomNavigationView = view.findViewById(R.id.disaster_bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.disaster_bottom_nav_peers);
        /* Set help_request_bottom_nav view item as checked */
        navigationView.setCheckedItem(R.id.nav_disaster);
        return view;
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Activity activity = getActivity();
            assert activity != null;
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(50);
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.disaster_bottom_nav_peers:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_PEERS);
                    if (fragment == null) {
                        fragment = disasterPeersList;
                    }
                    showFragment(fragment, TAG_FRAGMENT_PEERS);
                    activity.setTitle("Nearby Devices");
                    return true;

                case R.id.disaster_bottom_nav_chat:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_CHAT);
                    if (fragment == null) {
                        fragment = disasterFragmentChat;
                        if (disasterFragmentChat == null) {
                            Toast.makeText(App.context, "Cannot Open Chat without connecting to peer", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }
                    showFragment(fragment, TAG_FRAGMENT_CHAT);
                    activity.setTitle("Chat");
                    return true;

                case R.id.disaster_bottom_nav_find:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_FIND);
                    if (fragment == null) {
                        fragment = disasterFragmentFind;
                        if (disasterFragmentFind == null) {
                            Toast.makeText(App.context, "Cannot Open without connecting to peer", Toast.LENGTH_LONG).show();
                            return false;
                        }
                    }
                    showFragment(fragment, TAG_FRAGMENT_FIND);
                    activity.setTitle("Find Nearby Devices");
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getChildFragmentManager();
        getPermissions();
    }

    private void hideFragment(String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.hide(fragment);
            ft.commit();
        }
    }

    private void showFragment(Fragment fragment, String tag) {
        if (!fragment.equals(currentFragment)) {
            fragment.setEnterTransition(new Fade(Fade.IN));
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment.isAdded()) {
                ft.show(fragment);
            } else {
                ft.add(R.id.disaster_fragment_container, fragment, tag);
            }
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            currentFragment = fragment;
            ft.commitAllowingStateLoss();
        }
    }

    private void getPermissions() {
        Dexter.withActivity(getActivity())
                .withPermissions(
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CHANGE_WIFI_STATE,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                )
                .withListener(new BaseMultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (!report.areAllPermissionsGranted()) {
                            showSettingsDialog();
                        }
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Need Permissions");
        builder.setMessage("Some of the permissions were denied without which the disaster mode feature won't work. " +
                "Please grant them in app settings");
        builder.setPositiveButton("Goto Settings", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton("Go Back", (dialog, which) -> {
            dialog.cancel();
            Objects.requireNonNull(getActivity()).onBackPressed();
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", Objects.requireNonNull(getActivity()).getPackageName(), null);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setData(uri);
        startActivity(intent);
    }

    static void goToChat() {
        if (disasterFragmentChat == null) {
            disasterFragmentChat = DisasterFragmentChat.newInstance();
            disasterFragmentFind = DisasterFragmentFind.newInstance();
        }
        bottomNavigationView.setSelectedItemId(R.id.disaster_bottom_nav_chat);
    }

    static void disconnectedFromAllDevices() {
        disasterFragmentChat = null;
        disasterFragmentFind = null;
        bottomNavigationView.setSelectedItemId(R.id.disaster_bottom_nav_peers);
    }
}
