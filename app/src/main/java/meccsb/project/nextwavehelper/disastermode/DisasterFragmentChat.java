package meccsb.project.nextwavehelper.disastermode;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.nearby.connection.Payload;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.disastermode.entities.Message;
import meccsb.project.nextwavehelper.main.LocationListener;
import meccsb.project.nextwavehelper.main.SharedPref;

public class DisasterFragmentChat extends Fragment {
    @Inject
    LocationListener locationListener;

    private final static String TAG = "DisasterFragmentChat";

    private static MessagesListAdapter<Message> adapter;
    private static String senderName, senderPhone;

    static DisasterFragmentChat newInstance() {
        DisasterFragmentChat f = new DisasterFragmentChat();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPref sharedPref = new SharedPref(App.context);
        senderName = sharedPref.getString("userName", null);
        senderPhone = sharedPref.getString("userPhone", null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disaster_chat, container, false);
        MessageInput inputText = view.findViewById(R.id.disaster_chat_interaction_panel);
        MessagesList messagesList = view.findViewById(R.id.disaster_chat_messageList);

        MessageHolders holdersConfig = new MessageHolders()
                .setIncomingTextConfig(CustomIncomingTextMessageViewHolder.class,
                        R.layout.incoming_text_message_item);

        adapter = new MessagesListAdapter<>(senderPhone, holdersConfig, null);
        messagesList.setAdapter(adapter);

        inputText.setInputListener(input -> {
            sendMessage(String.valueOf(input));
            return true;
        });

        locationListener.observe(getViewLifecycleOwner(), location -> {
            Message locationUpdate = new Message(location, senderName);
            DisasterFragmentPeersList.send(Payload.fromBytes(convertToBytes(locationUpdate)));
        });

        return view;
    }

    private void sendMessage(String msg) {
        Log.v(TAG, "Send Message Start");
        Message message = new Message(msg, senderName, senderPhone);
        DisasterFragmentPeersList.send(Payload.fromBytes(convertToBytes(message)));
        adapter.addToStart(message, true);
    }

    static void receiveMessage(Message message) {
        adapter.addToStart(message, true);
    }

    private byte[] convertToBytes(Object object) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            ObjectOutput out = new ObjectOutputStream(bos);
            out.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return bos.toByteArray();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
