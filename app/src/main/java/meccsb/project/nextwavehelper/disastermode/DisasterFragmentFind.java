package meccsb.project.nextwavehelper.disastermode;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.BuildConfig;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.disastermode.entities.Message;
import meccsb.project.nextwavehelper.main.LocationListener;

public class DisasterFragmentFind extends Fragment {
    @Inject
    LocationListener locationListener;

    private final static String TAG = "DisasterFragmentFind";
    private MapView mapView;
    private MapboxMap myMapboxMap;
    private LatLng currentLatLng;
    private String mapStyle;

    private static MutableLiveData<List<Feature>> deviceLocations = new MutableLiveData<>();
    private static Map<String, Location> deviceUpdates = new HashMap<>();

    static DisasterFragmentFind newInstance() {
        DisasterFragmentFind f = new DisasterFragmentFind();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    static void locationUpdate(Message locationUpdate) {
        deviceUpdates.put(locationUpdate.getDeviceName(), locationUpdate.getLocation());
        List<Feature> features = new ArrayList<>();
        deviceUpdates.forEach((deviceId, location) -> {
            JsonObject properties = new JsonObject();
            properties.addProperty("label", deviceId);
            features.add(Feature.fromGeometry(
                    Point.fromLngLat(location.getLongitude(), location.getLatitude()),
                    properties, deviceId)
            );
        });
        deviceLocations.setValue(features);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(Objects.requireNonNull(getContext()), BuildConfig.MapBoxToken);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disaster_find, container, false);
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            mapStyle = Style.DARK;
        } else {
            mapStyle = Style.MAPBOX_STREETS;
        }
        // MapView
        mapView = view.findViewById(R.id.disaster_mode_mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(mapboxMap -> {
            myMapboxMap = mapboxMap;
            myMapboxMap.setStyle(mapStyle, style -> {
                enableLocationComponent(style);
                initLocationFAB(view);
                initMarkersLayer(style);
            });
        });
        return view;
    }

    private void initMarkersLayer(Style style) {
        // Add marker source
        deviceLocations.observe(getViewLifecycleOwner(), features -> {
            GeoJsonSource deviceMarkerSource = style.getSourceAs("marker-source");
            if (deviceMarkerSource != null) {
                deviceMarkerSource.setGeoJson(FeatureCollection.fromFeatures(features));
            }
        });
        style.addSource(new GeoJsonSource("marker-source"));
        // Add the marker image to map
        style.addImage("red-marker",
                BitmapFactory.decodeResource(
                        getResources(), R.drawable.mapbox_marker_icon_default));

        SymbolLayer symbolLayer = new SymbolLayer("marker-layer", "marker-source");
        symbolLayer.withProperties(
                PropertyFactory.iconImage("red-marker"),
                PropertyFactory.textField("{label}"),
                PropertyFactory.textOffset(new Float[]{0f, 0.6f}),
                PropertyFactory.textAnchor("top"),
                PropertyFactory.textSize(12f),
                PropertyFactory.textFont(new String[]{"Roboto Medium"})
        );
        style.addLayer(symbolLayer);
    }


    @SuppressWarnings("MissingPermission")
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Get an instance of the component
        LocationComponent locationComponent = myMapboxMap.getLocationComponent();
        // Activate with options
        locationComponent.activateLocationComponent(new LocationComponentActivationOptions
                .Builder(App.context, loadedMapStyle)
                .build());
        // Enable to make component visible
        locationComponent.setLocationComponentEnabled(true);
        // Set the component's render mode
        locationComponent.setRenderMode(RenderMode.COMPASS);
    }

    private void initLocationFAB(final View view) {
        FloatingActionButton locationFAB = view.findViewById(R.id.disaster_mode_map_location_fab);
        locationFAB.setVisibility(View.VISIBLE);
        locationFAB.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            locationListener.observe(getViewLifecycleOwner(), location -> {
                Log.d("Location", String.format("Lng: %f, Lat: %f", location.getLongitude(), location.getLatitude()));
                myMapboxMap.getLocationComponent().forceLocationUpdate(location);
                currentLatLng = new LatLng(location);
            });
            if (currentLatLng != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatLng)).zoom(15).build();
                myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
            } else {
                Snackbar.make(view, "Cannot get the current GPS coordinates", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void viewLatLng(double lat, double lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(15).build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
