package meccsb.project.nextwavehelper.disastermode;

import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AdvertisingOptions;
import com.google.android.gms.nearby.connection.ConnectionInfo;
import com.google.android.gms.nearby.connection.ConnectionLifecycleCallback;
import com.google.android.gms.nearby.connection.ConnectionResolution;
import com.google.android.gms.nearby.connection.ConnectionsClient;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.google.android.gms.nearby.connection.DiscoveredEndpointInfo;
import com.google.android.gms.nearby.connection.DiscoveryOptions;
import com.google.android.gms.nearby.connection.EndpointDiscoveryCallback;
import com.google.android.gms.nearby.connection.Payload;
import com.google.android.gms.nearby.connection.PayloadCallback;
import com.google.android.gms.nearby.connection.PayloadTransferUpdate;
import com.google.android.gms.nearby.connection.Strategy;
import com.google.android.material.snackbar.Snackbar;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.disastermode.entities.Endpoint;
import meccsb.project.nextwavehelper.disastermode.entities.Message;
import meccsb.project.nextwavehelper.main.SharedPref;

public class DisasterFragmentPeersList extends Fragment {
    private static final String TAG = "DisasterFragmentPeersList";
    private static final Strategy STRATEGY = Strategy.P2P_CLUSTER;
    private static String userName;

    private ProgressBar progressBar;
    private final PayloadCallback payloadCallback =
            new PayloadCallback() {
                @Override
                public void onPayloadReceived(@NonNull String endpointId, @NonNull Payload payload) {
                    // This always gets the full data of the payload. Will be null if it's not a BYTES
                    // payload. You can check the payload type with payload.getType().
                    Log.d(TAG, String.format("onPayloadReceived(endpointId=%s, payload=%s)", endpointId, payload));
                    onReceive(mEstablishedConnections.get(endpointId), payload);
                }

                @Override
                public void onPayloadTransferUpdate(@NonNull String endpointId, @NonNull PayloadTransferUpdate update) {
                    // Bytes payloads are sent as a single chunk, so you'll receive a SUCCESS update immediately
                    // after the call to onPayloadReceived().
                    Log.d(TAG,
                            String.format(
                                    "onPayloadTransferUpdate(endpointId=%s, update=%s)", endpointId, update));
                }
            };

    private Boolean isDiscovering = false;
    private Boolean isAdvertising = false;

    /**
     * The devices we have pending connections to.
     */
    private final Map<String, Endpoint> mPendingConnections = new HashMap<>();
    /**
     * The devices we are currently connected to.
     */
    private static final Map<String, Endpoint> mEstablishedConnections = new HashMap<>();

    private static final Map<String, Endpoint> mDiscoveredConnections = new HashMap<>();

    private String SERVICE_ID;
    private final MutableLiveData<List<Endpoint>> endPointsListLiveData = new MutableLiveData<>();
    private EndpointsListAdapter adapter = new EndpointsListAdapter();
    private ConnectionsClient connectionsClient;

    static DisasterFragmentPeersList newInstance() {
        DisasterFragmentPeersList f = new DisasterFragmentPeersList();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    private Button discoverBtn;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPref sharedPref = new SharedPref(App.context);
        userName = sharedPref.getString("userName", null);
        SERVICE_ID = Objects.requireNonNull(getActivity()).getPackageName();
        connectionsClient = Nearby.getConnectionsClient(App.context);
    }

    private final ConnectionLifecycleCallback connectionLifecycleCallback =
            new ConnectionLifecycleCallback() {
                @Override
                public void onConnectionInitiated(@NonNull String endpointId, @NonNull ConnectionInfo connectionInfo) {
                    // Automatically accept the connection on both sides.
                    Log.d(TAG, "Connection initiated : " + endpointId + " ," + connectionInfo.toString());
                    Endpoint endpoint = new Endpoint(endpointId, connectionInfo.getEndpointName());
                    mPendingConnections.put(endpointId, endpoint);
                    acceptConnection(endpoint);
                }

                @Override
                public void onConnectionResult(@NonNull String endpointId, @NonNull ConnectionResolution result) {
                    switch (result.getStatus().getStatusCode()) {
                        case ConnectionsStatusCodes.STATUS_OK:
                            // We're connected! Can now start sending and receiving data.
                            Log.d(TAG, "Connected : " + endpointId + " ," + result.toString());
                            connectedToEndpoint(mPendingConnections.remove(endpointId));
                            break;
                        case ConnectionsStatusCodes.STATUS_CONNECTION_REJECTED:
                            // The connection was rejected by one or both sides.
                            Log.w(TAG, "Connection rejected : " + endpointId + " ," + result.toString());
                            onConnectionFailed(mPendingConnections.remove(endpointId));
                            break;
                        case ConnectionsStatusCodes.STATUS_ERROR:
                            // The connection broke before it was able to be accepted.
                            onConnectionFailed(mPendingConnections.remove(endpointId));
                            break;
                        default:
                            // Unknown status code
                    }
                }

                @Override
                public void onDisconnected(@NonNull String endpointId) {
                    if (!mEstablishedConnections.containsKey(endpointId)) {
                        Log.w(TAG, "Unexpected disconnection from endpoint " + endpointId);
                        return;
                    }
                    disconnectedFromEndpoint(mEstablishedConnections.get(endpointId));
                }
            };
    private final EndpointDiscoveryCallback endpointDiscoveryCallback =
            new EndpointDiscoveryCallback() {
                @Override
                public void onEndpointFound(@NonNull String endpointId, DiscoveredEndpointInfo info) {
                    // An endpoint was found. We request a connection to it.
                    Endpoint endpoint = new Endpoint(endpointId, info.getEndpointName());
                    mDiscoveredConnections.put(endpointId, endpoint);
                    endPointsListLiveData.setValue(new ArrayList<>(mDiscoveredConnections.values()));
                    onEndpointDiscovered(endpoint);
                }

                @Override
                public void onEndpointLost(@NonNull String endpointId) {
                    // A previously discovered endpoint has gone away.
                }
            };

    static void send(Payload payload) {
        send(payload, mEstablishedConnections.keySet());
    }
    @Override
    public void onStart() {
        super.onStart();
        startAdvertising();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopAdvertising();
        stopDiscovery();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopAdvertising();
        stopDiscovery();
    }

    private static void send(Payload payload, Set<String> endpoints) {
        Nearby.getConnectionsClient(App.context)
                .sendPayload(new ArrayList<>(endpoints), payload)
                .addOnFailureListener(
                        e -> Log.w(TAG, "sendPayload() failed.", e));
    }

    private void startDiscovery() {
        if (isDiscovering) {
            return;
        }
        DiscoveryOptions discoveryOptions =
                new DiscoveryOptions.Builder().setStrategy(STRATEGY).build();
        connectionsClient.startDiscovery(SERVICE_ID, endpointDiscoveryCallback, discoveryOptions)
                .addOnSuccessListener(
                        (Void unused) -> {
                            // We're discovering!
                            discoverBtn.setText("Stop Discovery");
                            isDiscovering = true;
                            Toast.makeText(App.context, "Started Discovering!", Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.VISIBLE);
                        })
                .addOnFailureListener(
                        (Exception e) -> {
                            // We're unable to start discovering.
                            Toast.makeText(App.context, "Failed to start discovering!", Toast.LENGTH_SHORT).show();
                        });
    }

    private void stopAdvertising() {
        connectionsClient.stopAdvertising();
        isAdvertising = false;
    }

    private void stopDiscovery() {
        connectionsClient.stopDiscovery();
        discoverBtn.setText("Start Discovery");
        progressBar.setVisibility(View.GONE);
        isDiscovering = false;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_disaster_peers_list, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.disaster_peers_recycler_view);
        progressBar = view.findViewById(R.id.disaster_peers_list_progress_bar);

        endPointsListLiveData.observe(this, endpoints -> adapter.submitList(endpoints));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        discoverBtn = view.findViewById(R.id.disaster_peers_list_discover_btn);
        discoverBtn.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (isDiscovering) {
                stopDiscovery();
            } else {
                startDiscovery();
            }
        });
        return view;
    }

    private void connectedToEndpoint(Endpoint endpoint) {
        Log.d(TAG, String.format("connectedToEndpoint(endpoint=%s)", endpoint));
        mEstablishedConnections.put(endpoint.getId(), endpoint);
        onEndpointConnected(endpoint);
        DisasterFragment.goToChat();
    }

    private void disconnectedFromEndpoint(Endpoint endpoint) {
        Log.d(TAG, String.format("disconnectedFromEndpoint(endpoint=%s)", endpoint));
        mEstablishedConnections.remove(endpoint.getId());
        mDiscoveredConnections.remove(endpoint.getId());
        endPointsListLiveData.setValue(new ArrayList<>(mDiscoveredConnections.values()));
        onEndpointDisconnected(endpoint);
    }

    private void startAdvertising() {
        if (isAdvertising) {
            return;
        }
        isAdvertising = true;
        AdvertisingOptions advertisingOptions =
                new AdvertisingOptions.Builder().setStrategy(STRATEGY).build();
        connectionsClient.startAdvertising(
                userName, SERVICE_ID, connectionLifecycleCallback, advertisingOptions
        )
                .addOnSuccessListener(
                        (Void unused) -> Toast.makeText(App.context, "Started Advertising!", Toast.LENGTH_SHORT).show())
                .addOnFailureListener(
                        (Exception e) -> Toast.makeText(App.context, "Failed to start advertising!", Toast.LENGTH_SHORT).show());
    }

    private void onEndpointDiscovered(Endpoint endpoint) {
        // We found an advertiser!
        stopDiscovery();
        connectToEndpoint(endpoint);
    }

    private void onEndpointConnected(Endpoint endpoint) {
        Toast.makeText(
                App.context, String.format("Connected to %s", endpoint.getName()), Toast.LENGTH_SHORT)
                .show();
        stopDiscovery();
    }

    private void onEndpointDisconnected(Endpoint endpoint) {
        Toast.makeText(
                App.context, String.format("Disconnected from %s", endpoint.getName()), Toast.LENGTH_SHORT)
                .show();
        if (mEstablishedConnections.size() == 0) {
            DisasterFragment.disconnectedFromAllDevices();
            startDiscovery();
        }
    }

    private void connectToEndpoint(final Endpoint endpoint) {
        Log.v(TAG, "Sending a connection request to endpoint " + endpoint);
        if (getView() != null) {
            Snackbar.make(getView(), String.format("Sending connection request to \"%s\"", endpoint.getName()), Snackbar.LENGTH_LONG).show();
        }
        // Ask to connect
        connectionsClient
                .requestConnection(userName, endpoint.getId(), connectionLifecycleCallback)
                .addOnFailureListener(
                        e -> {
                            Log.w(TAG, "requestConnection() failed.", e);
                            onConnectionFailed(endpoint);
                        });
    }

    /**
     * Accepts a connection request.
     */
    private void acceptConnection(final Endpoint endpoint) {
        connectionsClient
                .acceptConnection(endpoint.getId(), payloadCallback)
                .addOnFailureListener(
                        (Exception e) -> Log.w(TAG, "acceptConnection() failed.", e));
    }

    private void onReceive(Endpoint endpoint, Payload payload) {
        Message receivedMessage = (Message) convertFromBytes(payload.asBytes());

        if (receivedMessage.isTextMessage())
            DisasterFragmentChat.receiveMessage(receivedMessage);
        else
            DisasterFragmentFind.locationUpdate(receivedMessage);

        Map<String, Endpoint> connectedEndpoints = new HashMap<>(mEstablishedConnections);
        connectedEndpoints.remove(endpoint.getId());
        if (connectedEndpoints.size() > 0)
            send(payload, connectedEndpoints.keySet());
    }

    private void onConnectionFailed(Endpoint endpoint) {
        if (getView() != null) {
            Snackbar.make(getView(), String.format("Connection to \"%s\" failed!", endpoint.getName()), Snackbar.LENGTH_SHORT).show();
        }
        // Let's try someone else.
        startDiscovery();
    }

    private static Object convertFromBytes(byte[] bytes) {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        Object obj = new Object();
        try {
            ObjectInput in = new ObjectInputStream(bis);
            obj = in.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return obj;
    }
}
