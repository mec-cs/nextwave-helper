package meccsb.project.nextwavehelper.disastermode;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.disastermode.entities.Endpoint;

public class EndpointsListAdapter extends ListAdapter<Endpoint, EndpointsListAdapter.EndpointHolder> {
    EndpointsListAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<Endpoint> DIFF_CALLBACK = new DiffUtil.ItemCallback<Endpoint>() {
        @Override
        public boolean areItemsTheSame(@NonNull Endpoint oldItem, @NonNull Endpoint newItem) {
            return oldItem.getId().equals(newItem.getId());
        }

        @Override
        public boolean areContentsTheSame(@NonNull Endpoint oldItem, @NonNull Endpoint newItem) {
            return oldItem.getName().equals(newItem.getName());
        }
    };

    @NonNull
    @Override
    public EndpointHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.disaster_endpoint_item, parent, false);
        return new EndpointHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull EndpointsListAdapter.EndpointHolder holder, int position) {
        Endpoint currentEndpoint = getItem(position);
        holder.deviceId.setText(String.format("Endpoint ID : %s", currentEndpoint.getId()));
        holder.deviceName.setText(currentEndpoint.getName());
    }

    class EndpointHolder extends RecyclerView.ViewHolder {
        private final TextView deviceName;
        private final TextView deviceId;

        EndpointHolder(@NonNull View itemView) {
            super(itemView);
            deviceName = itemView.findViewById(R.id.disaster_item_device_name);
            deviceId = itemView.findViewById(R.id.disaster_item_device_id);
        }
    }
}
