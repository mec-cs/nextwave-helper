package meccsb.project.nextwavehelper.disastermode.entities;

import com.stfalcon.chatkit.commons.models.IUser;

import java.io.Serializable;

public class Author implements Serializable, IUser {
    private String name;
    private String id;

    public Author(String name, String id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return null;
    }
}
