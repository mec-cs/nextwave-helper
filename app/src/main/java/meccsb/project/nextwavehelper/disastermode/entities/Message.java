package meccsb.project.nextwavehelper.disastermode.entities;

import android.location.Location;

import com.stfalcon.chatkit.commons.models.IMessage;
import com.stfalcon.chatkit.commons.models.IUser;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class Message implements Serializable, IMessage {
    private String id;
    private String text;
    private Author author;
    private Date timestamp;
    private Boolean isTextMessage;
    private Double lat, lng;
    private String deviceName;

    public Message(String text, String senderName, String senderPhone) {
        this.isTextMessage = true;
        this.text = text;
        this.author = new Author(senderName, senderPhone);
        this.timestamp = Calendar.getInstance().getTime();
        this.id = senderPhone + timestamp.toString();
    }

    public Message(Location location, String deviceName) {
        this.isTextMessage = false;
        this.lat = location.getLatitude();
        this.lng = location.getLongitude();
        this.deviceName = deviceName;
    }


    @Override
    public String getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    @Override
    public IUser getUser() {
        return author;
    }

    @Override
    public Date getCreatedAt() {
        return timestamp;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isTextMessage() {
        return isTextMessage;
    }

    public Location getLocation() {
        Location location = new Location("");
        location.setLongitude(lng);
        location.setLatitude(lat);
        return location;
    }

    public String getDeviceName() {
        return deviceName;
    }
}
