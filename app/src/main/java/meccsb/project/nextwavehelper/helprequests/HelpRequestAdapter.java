package meccsb.project.nextwavehelper.helprequests;

import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;

public class HelpRequestAdapter extends ListAdapter<HelpRequest, HelpRequestAdapter.HelpRequestHolder> {
    private OnItemClickListener listener;
    HelpRequestAdapter() {
        super(DIFF_CALLBACK);
    }
    private static final DiffUtil.ItemCallback<HelpRequest> DIFF_CALLBACK = new DiffUtil.ItemCallback<HelpRequest>() {
        @Override
        public boolean areItemsTheSame(@NonNull HelpRequest oldItem, @NonNull HelpRequest newItem) {
            return oldItem.getRequestID() == newItem.getRequestID();
        }

        @Override
        public boolean areContentsTheSame(@NonNull HelpRequest oldItem, @NonNull HelpRequest newItem) {
            return oldItem.getTime().equals(newItem.getTime()) &&
                    oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getPhone().equals(newItem.getPhone())&&
                    oldItem.getLongitude().equals(newItem.getLongitude())&&
                    oldItem.getLatitude().equals(newItem.getLatitude())&&
                    oldItem.getLocation().equals(newItem.getLocation())&&
                    oldItem.getNeeds().equals(newItem.getNeeds())&&
                    oldItem.getDetails().equals(newItem.getDetails()) &&
                    oldItem.getStatus() == newItem.getStatus();
        }
    };
    @NonNull
    @Override
    public HelpRequestHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.help_request_item, parent, false);
        return new HelpRequestHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final HelpRequestAdapter.HelpRequestHolder holder, int position) {
        HelpRequest currentHelpRequest = getItem(position);
        holder.timeText.setText(currentHelpRequest.getTime());
        holder.nameText.setText(currentHelpRequest.getName());
        holder.phoneText.setText(String.format("Phone: %s", currentHelpRequest.getPhone()));
        holder.locationText.setText(String.format("Location: %s", currentHelpRequest.getLocation()));
        holder.needsText.setText(String.format("Needs: %s", currentHelpRequest.getNeeds()));
        holder.workerName.setText(String.format("Worker Name: %s", currentHelpRequest.getWorkerName()));
        holder.workerNumber.setText(String.format("Worker Number: %s", currentHelpRequest.getWorkerNumber()));

        if (currentHelpRequest.getDetails().isEmpty()) {
            holder.detailsText.setVisibility(View.GONE);
        } else {
            holder.detailsText.setText(String.format("Details: %s", currentHelpRequest.getDetails()));
        }

        switch (currentHelpRequest.getStatus()){
            case 0: // Low - Green
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_maps);
                break;
            case 1: // Medium - Orange
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_help_requests);
                break;
            case 2: // High - Red
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_disaster_mode);
                holder.expandBtn.setVisibility(View.GONE);
                holder.relativeLayout.setOnClickListener(null);
                break;
        }
    }

    void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {
        void onItemLongClick(HelpRequest helpRequest, View view);
    }

    class HelpRequestHolder extends RecyclerView.ViewHolder{
        private final RelativeLayout relativeLayout;
        private final TextView timeText;
        private final TextView nameText;
        private final TextView phoneText;
        private final TextView locationText;
        private final TextView needsText;
        private final TextView detailsText;
        private final TextView workerName;
        private final TextView workerNumber;
        private final ImageView expandBtn;
        private Boolean isExpFlag;

        HelpRequestHolder(@NonNull View itemView) {
            super(itemView);
            isExpFlag = false;
            relativeLayout = itemView.findViewById(R.id.help_request_item_layout);
            timeText = itemView.findViewById(R.id.help_request_item_time);
            nameText = itemView.findViewById(R.id.help_request_item_username);
            phoneText = itemView.findViewById(R.id.help_request_item_phone_number);
            locationText = itemView.findViewById(R.id.help_request_item_location);
            needsText = itemView.findViewById(R.id.help_request_item_needs);
            detailsText = itemView.findViewById(R.id.help_request_item_details);
            workerName = itemView.findViewById(R.id.help_request_item_worker_name);
            workerNumber = itemView.findViewById(R.id.help_request_item_worker_number);
            expandBtn = itemView.findViewById(R.id.help_request_item_expandview);
            itemView.setOnClickListener(v -> {
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                if (isExpFlag) {
                    isExpFlag = false;
                    workerName.setVisibility(View.GONE);
                    workerNumber.setVisibility(View.GONE);
                    expandBtn.setImageResource(R.drawable.ic_expandview);
                } else {
                    isExpFlag = true;
                    workerName.setVisibility(View.VISIBLE);
                    workerNumber.setVisibility(View.VISIBLE);
                    expandBtn.setImageResource(R.drawable.ic_collapseview);
                }
            });
            itemView.setOnLongClickListener(v -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemLongClick(getItem(position), v);
                }
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                return true;
            });
        }
    }
}
