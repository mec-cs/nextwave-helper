package meccsb.project.nextwavehelper.helprequests;

import android.content.Context;
import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.SharedPref;
import meccsb.project.nextwavehelper.main.TextValidator;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import meccsb.project.nextwavehelper.roomdb.viewmodels.HelpRequestViewModel;

public class HelpRequestsAddFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    private HelpRequestViewModel helpRequestViewModel;
    private TextView latTextView, lngTextView;
    private static final String TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP = "fragment_help_request_place_picker_map";
    private Double helpRequestLat=0.0, helpRequestLng=0.0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helpRequestViewModel = ViewModelProviders.of(this, viewModelFactory).get(HelpRequestViewModel.class);

        // disable search bar
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_help_requests_add, container, false);
        final ProgressBar progressBar = view.findViewById(R.id.help_request_form_progress_bar);

        final TextInputLayout textInputName = view.findViewById(R.id.help_request_form_name);
        final TextInputLayout textInputPhone = view.findViewById(R.id.help_request_form_phone);
        final TextInputLayout textInputLocation = view.findViewById(R.id.help_request_form_location);
        final TextInputLayout textInputDetails = view.findViewById(R.id.help_request_form_details);

        final CheckBox checkBoxWater = view.findViewById(R.id.help_request_form_water);
        final CheckBox checkBoxClothes = view.findViewById(R.id.help_request_form_clothes);
        final CheckBox checkBoxRescue = view.findViewById(R.id.help_request_form_rescue);
        final CheckBox checkBoxToiletries = view.findViewById(R.id.help_request_form_toiletries);
        final CheckBox checkBoxUtensils = view.findViewById(R.id.help_request_form_utensils);
        final CheckBox checkBoxFood = view.findViewById(R.id.help_request_form_food);
        final CheckBox checkBoxMedicine = view.findViewById(R.id.help_request_form_medicine);

        final Button submitBtn = view.findViewById(R.id.help_request_form_submit);
        final Button pickLocation = view.findViewById(R.id.help_request_form_pick_loc);

        SharedPref sharedPref = new SharedPref(App.context);

        getActivity().setTitle("Add a Help Request");

        textInputName.getEditText().setText(sharedPref.getString("userName", null));
        textInputPhone.getEditText().setText(sharedPref.getString("userPhone", null));

        latTextView = view.findViewById(R.id.help_request_form_lat);
        lngTextView = view.findViewById(R.id.help_request_form_lng);

        textInputName.getEditText().addTextChangedListener(
                new TextValidator(textInputName.getEditText(), "^[A-Za-z \\.]+$", "name"));
        textInputPhone.getEditText().addTextChangedListener(
                new TextValidator(textInputPhone.getEditText(), "^[+]?[0-9]{10,13}$", "phone number"));

        helpRequestViewModel.getStatus().observe(getViewLifecycleOwner(), s -> {
            if (s.equals("SUCCESS")) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(), "Successfully Added!", Toast.LENGTH_LONG).show();
                Objects.requireNonNull(getFragmentManager()).popBackStack();
                helpRequestViewModel.getStatus().setValue("");
                helpRequestViewModel.refreshFromWeb();
            } else if (s.equals("FAILED")) {
                progressBar.setVisibility(View.INVISIBLE);
                Snackbar.make(view, "Failed to add, Please try again later.", Snackbar.LENGTH_LONG).show();
                helpRequestViewModel.getStatus().setValue("");
            }
        });

        helpRequestViewModel.getCurrLoc().observe(getViewLifecycleOwner(), location -> {
            latTextView.setText(String.format("%.2f",location.getLatitude()));
            lngTextView.setText(String.format("%.2f",location.getLongitude()));
            helpRequestLat = location.getLatitude();
            helpRequestLng = location.getLongitude();
        });



        submitBtn.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (validateEmpty(textInputName) || validateEmpty(textInputPhone) || validateEmpty(textInputLocation)
                    || validateLocationEmpty(helpRequestLat, helpRequestLng)){
                return;
            }

            submitBtn.setEnabled(false);    //unsure if its working

            final StringBuilder needs = new StringBuilder(100);
            final List<CheckBox> needsList = new ArrayList<>();
            needsList.add(checkBoxRescue);
            needsList.add(checkBoxFood);
            needsList.add(checkBoxWater);
            needsList.add(checkBoxClothes);
            needsList.add(checkBoxMedicine);
            needsList.add(checkBoxUtensils);
            needsList.add(checkBoxToiletries);

            for(CheckBox item:needsList) {
                if(item.isChecked()) {
                    needs.append(item.getText());
                    needs.append(", ");
                }
            }
            if(needs.length() > 0) {
                needs.setLength(needs.length() - 2);
            }

            HelpRequest helpRequest = new HelpRequest();
            helpRequest.setName(getStringFromTextInputLayout(textInputName));
            helpRequest.setPhone(getStringFromTextInputLayout(textInputPhone));
            helpRequest.setLocation(getStringFromTextInputLayout(textInputLocation));
            helpRequest.setNeeds(needs.toString());
            helpRequest.setStatus(2);
            helpRequest.setLatitude(String.valueOf(helpRequestLat));
            helpRequest.setLongitude(String.valueOf(helpRequestLng));
            helpRequest.setDetails(getStringFromTextInputLayout(textInputDetails));

            helpRequestViewModel.postToWeb(helpRequest);
            progressBar.setVisibility(View.VISIBLE);
        });

        pickLocation.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            showFragment(new HelpRequestsAddPlacePickerFragment());
        });

        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.menu_search);
        item.setVisible(false);
    }

    private String getStringFromTextInputLayout(TextInputLayout textInputLayout) {
        return textInputLayout.getEditText().getText().toString().trim();
    }

    private boolean validateLocationEmpty(double lat, double lng) {
        if(lat == 0 && lng ==0) {
            Toast.makeText(getContext(), "Please pick a location", Toast.LENGTH_LONG).show();
            return true;
        }
        else
            return  false;
    }

    private boolean validateEmpty(TextInputLayout textInputLayout) {
        String text = getStringFromTextInputLayout(textInputLayout);
        if (textInputLayout.getEditText().getError() != null) {
            return true;
        }
        if (text.isEmpty()) {
            textInputLayout.setError("Field can't be empty");
            return true;
        } else {
            textInputLayout.setError(null);
            return false;
        }
    }

    private void showFragment(Fragment newFragment) {
        newFragment.setEnterTransition(new Fade(Fade.IN));
        FragmentTransaction fragmentTransaction = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        fragmentTransaction.hide(this);
        fragmentTransaction.add(R.id.fragment_container, newFragment, TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
