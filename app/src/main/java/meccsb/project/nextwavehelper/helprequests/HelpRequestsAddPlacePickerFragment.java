package meccsb.project.nextwavehelper.helprequests;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.ui.PlaceAutocompleteFragment;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.ui.PlaceSelectionListener;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.BuildConfig;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.LocationListener;
import meccsb.project.nextwavehelper.roomdb.viewmodels.HelpRequestViewModel;

public class HelpRequestsAddPlacePickerFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocationListener locationListener;
    private HelpRequestViewModel helpRequestViewModel;

    private static final String TAG_FRAGMENT_SEARCH = "fragment_search";
    private static final String TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP = "fragment_help_request_place_picker_map";

    private MapView mapView;
    private MapboxMap myMapboxMap;
    private LatLng currentLatLng;
    private String mapStyle;

    private PlaceAutocompleteFragment autocompleteFragment = PlaceAutocompleteFragment.newInstance(BuildConfig.MapBoxToken);
    private Fragment currentFragment;
    private FragmentManager fragmentManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helpRequestViewModel = ViewModelProviders.of(this, viewModelFactory).get(HelpRequestViewModel.class);
        Mapbox.getInstance(Objects.requireNonNull(getContext()), BuildConfig.MapBoxToken);
        fragmentManager = getFragmentManager();
        currentFragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_help_requests_add_place_picker, container, false);
        final Button button = view.findViewById(R.id.help_request_form_place_picker_submit);

        int searchColor;
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            mapStyle = Style.DARK;
            searchColor = Color.BLACK;
        } else {
            mapStyle = Style.MAPBOX_STREETS;
            searchColor = Color.WHITE;
        }

        // Limit search results to India
        PlaceOptions placeOptions = PlaceOptions.builder()
                .country("IN")
                .backgroundColor(searchColor)
                .toolbarColor(searchColor)
                .build();
        autocompleteFragment = PlaceAutocompleteFragment.newInstance(BuildConfig.MapBoxToken, placeOptions);

        // MapView
        mapView = view.findViewById(R.id.help_request_place_picker_mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(@NonNull final MapboxMap mapboxMap) {
                myMapboxMap = mapboxMap;
                myMapboxMap.setStyle(mapStyle, new Style.OnStyleLoaded() {
                    @Override
                    public void onStyleLoaded(@NonNull Style style) {
                        enableLocationComponent(style);
                        initLocationFAB(view);
                        initSearchFragment();
                        initSearchFAB(view);
                    }
                });
            }
        });


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                Location loc = new Location("");
                final LatLng mapTargetLatLng = myMapboxMap.getCameraPosition().target;
                loc.setLatitude(mapTargetLatLng.getLatitude());
                loc.setLongitude(mapTargetLatLng.getLongitude());
                helpRequestViewModel.getCurrLoc().setValue(loc);
                getFragmentManager().popBackStack();
            }
        });

        // disable search bar
        setHasOptionsMenu(false);

        return view;
    }

    private void viewSearchResult(BoundingBox boundingBox) {
        LatLng northEast, southWest;
        northEast = new LatLng(boundingBox.northeast().latitude(), boundingBox.northeast().longitude());
        southWest = new LatLng(boundingBox.southwest().latitude(), boundingBox.southwest().longitude());
        LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(northEast) // Northeast
                .include(southWest) // Southwest
                .build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50), 3000);
    }

    @SuppressWarnings("MissingPermission")
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Get an instance of the component
        LocationComponent locationComponent = myMapboxMap.getLocationComponent();
        // Activate with options
        locationComponent.activateLocationComponent(new LocationComponentActivationOptions
                .Builder(App.context, loadedMapStyle)
                .build());        // Enable to make component visible
        locationComponent.setLocationComponentEnabled(true);
        // Set the component's render mode
        locationComponent.setRenderMode(RenderMode.COMPASS);
    }

    private void initLocationFAB(final View view) {
        FloatingActionButton locationFAB = view.findViewById(R.id.help_request_place_picker_map_location_fab);
        locationFAB.setVisibility(View.VISIBLE);
        locationFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                locationListener.observe(getViewLifecycleOwner(), new Observer<Location>() {
                    @Override
                    public void onChanged(Location location) {
                        Log.d("Location", String.format("Lng: %f, Lat: %f", location.getLongitude(), location.getLatitude()));
                        myMapboxMap.getLocationComponent().forceLocationUpdate(location);
                        currentLatLng = new LatLng(location);
                    }
                });
                if (currentLatLng != null) {
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatLng)).zoom(15).build();
                    myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
                } else {
                    Snackbar.make(view, "Cannot get the current GPS coordinates", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void viewLatLng(double lat, double lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(15).build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
    }

    private void initSearchFragment() {
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {

            @Override
            public void onPlaceSelected(CarmenFeature carmenFeature) {
                Fragment fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP);

                Point point = carmenFeature.center();
                if (carmenFeature.bbox() != null) {
                    viewSearchResult(carmenFeature.bbox());
                } else if (point != null) {
                    viewLatLng(point.latitude(), point.longitude());
                }
                // hide the search fragment and show the map.
                showFragment(fragment, TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP);
            }

            @Override
            public void onCancel() {
                Fragment fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP);
                showFragment(fragment, TAG_FRAGMENT_HELP_REQUEST_PLACE_PICKER_MAP);
            }
        });
    }

    private void initSearchFAB(View view) {
        FloatingActionButton searchFAB = view.findViewById(R.id.help_request_place_picker_map_search_fab);
        searchFAB.setVisibility(View.VISIBLE);
        searchFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                Fragment fragment;
                fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_SEARCH);
                if (fragment == null) {
                    fragment = autocompleteFragment;
                }
                showFragment(fragment, TAG_FRAGMENT_SEARCH);
            }
        });
    }

    private void showFragment(Fragment fragment, String tag) {
        if (!fragment.equals(currentFragment)) {
            fragment.setEnterTransition(new Fade(Fade.IN));
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment.isAdded()) {
                ft.show(fragment);
            } else {
                ft.add(R.id.fragment_container, fragment, tag);
            }
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            ft.addToBackStack(null);
            currentFragment = fragment;
            ft.commit();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }

}
