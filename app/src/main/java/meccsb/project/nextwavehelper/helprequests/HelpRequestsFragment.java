package meccsb.project.nextwavehelper.helprequests;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Fade;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.SharedPref;

public class HelpRequestsFragment extends Fragment {
    private static final String TAG_FRAGMENT_ALL = "fragment_all";
    private static final String TAG_FRAGMENT_REGISTERED = "fragment_registered";
    private static final String TAG_FRAGMENT_ASSIGNED = "fragment_assigned";

    private FragmentManager fragmentManager;
    private Fragment currentFragment;

    private Fragment helpRequestsListFragment = HelpRequestsListFragment.newInstance();
    private Fragment helpRequestsListRegFragment = HelpRequestsListRegFragment.newInstance();
    private Fragment helpRequestsListAssFragment = HelpRequestsListAssFragment.newInstance();
    private FloatingActionButton fab;

    private SharedPref sharedPref;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help_requests, container, false);
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);

        fab = view.findViewById(R.id.missing_person_add_fab);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                replaceFragments(new HelpRequestsAddFragment());
            }
        });

        hideFragment(TAG_FRAGMENT_ALL);
        hideFragment(TAG_FRAGMENT_ASSIGNED);
        hideFragment(TAG_FRAGMENT_REGISTERED);

        BottomNavigationView bottomNavigationView = view.findViewById(R.id.help_request_bottom_nav);

        if (sharedPref.getBoolean("isRescueWorker", false)) {
            bottomNavigationView.inflateMenu(R.menu.help_request_bottom_nav_rescue_worker);
        } else {
            bottomNavigationView.inflateMenu(R.menu.help_request_bottom_nav);
        }

        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.help_request_bottom_nav_all);
        /* Set title text according to the fragment*/
        getActivity().setTitle(R.string.title_help_requests);
        /* Set help_request_bottom_nav view item as checked */
        navigationView.setCheckedItem(R.id.nav_help);
        return view;
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Activity activity = getActivity();
            assert activity != null;
            Fragment fragment;
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(50);
            switch (item.getItemId()) {
                case R.id.help_request_bottom_nav_all:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_ALL);
                    if (fragment == null) {
                        fragment = helpRequestsListFragment;
                    }
                    showFragment(fragment, TAG_FRAGMENT_ALL);
                    activity.setTitle("Help Requests");
                    fab.setVisibility(View.VISIBLE);
                    return true;

                case R.id.help_request_bottom_nav_reg:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_REGISTERED);
                    if (fragment == null) {
                        fragment = helpRequestsListRegFragment;
                    }
                    showFragment(fragment, TAG_FRAGMENT_REGISTERED);
                    activity.setTitle("My Requests");
                    fab.setVisibility(View.GONE);
                    return true;

                case R.id.help_request_bottom_nav_ass:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_ASSIGNED);
                    if (fragment == null) {
                        fragment = helpRequestsListAssFragment;
                    }
                    showFragment(fragment, TAG_FRAGMENT_ASSIGNED);
                    fab.setVisibility(View.GONE);
                    activity.setTitle("Assigned Requests");
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getChildFragmentManager();
        sharedPref = new SharedPref(App.context);
    }

    private void hideFragment(String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.hide(fragment);
            ft.commit();
        }
    }

    private void replaceFragments(Fragment newFragment) {
        newFragment.setEnterTransition(new Fade(Fade.IN));
        FragmentTransaction fragmentTransaction = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.add(R.id.fragment_container, newFragment);
        fragmentTransaction.hide(this);
        fragmentTransaction.commit();
    }


    private void showFragment(Fragment fragment, String tag) {
        if (!fragment.equals(currentFragment)) {
            fragment.setEnterTransition(new Fade(Fade.IN));
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment.isAdded()) {
                ft.show(fragment);
            } else {
                ft.add(R.id.help_request_fragment_container, fragment, tag);
            }
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            currentFragment = fragment;
            ft.commit();
        }
    }
}
