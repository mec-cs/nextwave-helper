package meccsb.project.nextwavehelper.helprequests;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.api.HelpRequestWebservice;
import meccsb.project.nextwavehelper.main.SharedPref;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import meccsb.project.nextwavehelper.roomdb.viewmodels.HelpRequestViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpRequestsListAssFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Inject
    HelpRequestWebservice webservice;

    private HelpRequestViewModel helpRequestViewModel;
    private final HelpRequestAdapter adapter = new HelpRequestAdapter();

    public static HelpRequestsListAssFragment newInstance() {
        HelpRequestsListAssFragment f = new HelpRequestsListAssFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPref sharedPref = new SharedPref(App.context);

        helpRequestViewModel = ViewModelProviders.of(this, viewModelFactory).get(HelpRequestViewModel.class);
        helpRequestViewModel.getAssignedHelpRequests(sharedPref.getString("userPhone", null)).observe(this, adapter::submitList);
    }

    @Override
    public void onResume() {
        super.onResume();
        helpRequestViewModel.refreshFromWeb();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help_requests_registered, container, false);

        /* To get the search button in menu*/
        setHasOptionsMenu(false);

        RecyclerView recyclerView = view.findViewById(R.id.help_request_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener((helpRequest, view1) -> {
            Log.d("TAG", "Selected: " + helpRequest.getName());
            showPopupMenu(helpRequest, view1);
        });
        Toast.makeText(App.context, "Long-press to view the help request on Google Maps", Toast.LENGTH_LONG).show();
        return view;
    }

    private void showPopupMenu(final HelpRequest helpRequest, View view) {
        PopupMenu popup = new PopupMenu(App.context, view);
        popup.setGravity(Gravity.END);
        popup.inflate(R.menu.helprequest_context);

        // remove the mark as completed option if the status is completed already.
        if (helpRequest.getStatus() == 0) {
            popup.getMenu().removeItem(R.id.completed_status);
        }

        popup.setOnMenuItemClickListener(item -> {
            view.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            switch (item.getItemId()) {
                case R.id.completed_status:
                    //webservice
                    webservice.markAsCompleted(helpRequest).enqueue(new Callback<Void>() {
                        @Override
                        public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                            helpRequestViewModel.refreshFromWeb();
                            Toast.makeText(App.context, "Marked as completed", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                            Toast.makeText(App.context, "Marking as Completed Failed!", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return true;
                case R.id.view_on_maps:
                    Uri gmmIntentUri = Uri.parse(String.format("geo:0,0?q=%s,%s(%s)", helpRequest.getLatitude(), helpRequest.getLongitude(), helpRequest.getName()));
                    Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                    mapIntent.setPackage("com.google.android.apps.maps");
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    mapIntent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                    startActivity(mapIntent);
                    return true;
            }
            return false;
        });
        popup.show();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
