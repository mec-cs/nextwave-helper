package meccsb.project.nextwavehelper.helprequests;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import meccsb.project.nextwavehelper.roomdb.viewmodels.HelpRequestViewModel;

public class HelpRequestsListFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private HelpRequestViewModel helpRequestViewModel;
    private final HelpRequestAdapter adapter = new HelpRequestAdapter();
    static HelpRequestsListFragment newInstance() {
        HelpRequestsListFragment f = new HelpRequestsListFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        helpRequestViewModel = ViewModelProviders.of(this, viewModelFactory).get(HelpRequestViewModel.class);
    }
    @Override
    public void onResume() {
        super.onResume();
        helpRequestViewModel.refreshFromWeb();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help_requests_list, container, false);

        /* To get the search button in menu*/
        setHasOptionsMenu(true);

        final RecyclerView recyclerView = view.findViewById(R.id.help_request_recycler_view);
        final ProgressBar progressBar = view.findViewById(R.id.help_request_progressBar);
        final LinearLayout mainContent = view.findViewById(R.id.help_request_list_main_content);
        helpRequestViewModel.getAllHelpRequests().observe(this, new Observer<List<HelpRequest>>() {
            @Override
            public void onChanged(List<HelpRequest> helpRequests) {
                adapter.submitList(helpRequests);
                if(helpRequests.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    mainContent.setVisibility(View.VISIBLE);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setQueryHint("Search Help Requests");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                helpRequestViewModel.setQuery(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                helpRequestViewModel.setQuery(newText);
                return true;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger(){
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
