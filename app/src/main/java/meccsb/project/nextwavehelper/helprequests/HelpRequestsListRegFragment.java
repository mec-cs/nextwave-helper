package meccsb.project.nextwavehelper.helprequests;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.SharedPref;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import meccsb.project.nextwavehelper.roomdb.viewmodels.HelpRequestViewModel;

public class HelpRequestsListRegFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private HelpRequestViewModel helpRequestViewModel;
    private SharedPref sharedPref;
    private final HelpRequestAdapter adapter = new HelpRequestAdapter();

    public static HelpRequestsListRegFragment newInstance() {
        HelpRequestsListRegFragment f = new HelpRequestsListRegFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPref = new SharedPref(getContext());

        helpRequestViewModel = ViewModelProviders.of(this, viewModelFactory).get(HelpRequestViewModel.class);
        helpRequestViewModel.getRegisteredRequests(sharedPref.getString("userPhone", null)).observe(this, new Observer<List<HelpRequest>>() {
            @Override
            public void onChanged(List<HelpRequest> helpRequests) {
                adapter.submitList(helpRequests);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        helpRequestViewModel.refreshFromWeb();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_help_requests_registered, container, false);

        /* To get the search button in menu*/
        setHasOptionsMenu(false);

        RecyclerView recyclerView = view.findViewById(R.id.help_request_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
