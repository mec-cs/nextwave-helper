package meccsb.project.nextwavehelper.intro;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.MainActivity;
import meccsb.project.nextwavehelper.main.SharedPref;

public class IntroActivity extends AppCompatActivity {

    private LinearLayout mDotLayout;
    private RelativeLayout introLayout;
    private TextView[] mDots;

    private Button mFinBtn;
    private final int[] backgrounds = {
            R.drawable.gradient_announcement,
            R.drawable.gradient_maps,
            R.drawable.gradient_help_requests,
            R.drawable.gradient_missing_persons,
            R.drawable.gradient_disaster_mode
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        introLayout = findViewById(R.id.introLayout);
        ViewPager mSlideViewPager = findViewById(R.id.slideViewPager);
        mDotLayout = findViewById(R.id.dotsLayout);

        mFinBtn = findViewById(R.id.finBtn);

        SliderAdapter sliderAdapter = new SliderAdapter(this);

        mSlideViewPager.setAdapter(sliderAdapter);

        addDotsIndicator(0);
        mSlideViewPager.addOnPageChangeListener(viewListener);
    }

    private void addDotsIndicator(int position) {

        mDots = new TextView[5];
        mDotLayout.removeAllViews();
        for (int i = 0; i < mDots.length; i++) {
            mDots[i] = new TextView(this);
            mDots[i].setText("\u2022");
            mDots[i].setTextSize(35);
            mDots[i].setTextColor(Color.parseColor("#cccccc"));

            mDotLayout.addView(mDots[i]);

        }
        if (mDots.length > 0)
            mDots[position].setTextColor(Color.parseColor("#ffffff"));

    }

    private final ViewPager.OnPageChangeListener viewListener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int i, float v, int i1) {
        }

        @Override
        public void onPageSelected(int i) {
            introLayout.setBackgroundResource(backgrounds[i]);
            addDotsIndicator(i);
            if (i == mDots.length - 1) {
                mFinBtn.setEnabled(true);
                mFinBtn.setText(R.string.fin_btn_text);
                mFinBtn.setVisibility(Button.VISIBLE);
                mFinBtn.setOnClickListener(v -> {
                    v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                    openLoginActivity();
                });
            } else {
                mFinBtn.setVisibility(Button.INVISIBLE);
            }
        }

        @Override
        public void onPageScrollStateChanged(int i) {
        }
    };

    private void openLoginActivity() {
        SharedPref sharedPref = new SharedPref(this);
        sharedPref.setBoolean("isFirstRun", false);

        // useful if we are running the intro again from settings.
        if (sharedPref.getBoolean("isWelcomeFormFilled", false)) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
            return;
        }

        Intent intent = new Intent(this, WelcomeActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }
}
