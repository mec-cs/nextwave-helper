package meccsb.project.nextwavehelper.intro;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import meccsb.project.nextwavehelper.R;

class SliderAdapter extends PagerAdapter {

    private final Context context;

    SliderAdapter(Context context){
        this.context = context;

    }

    private final int[] slide_images = {
            R.drawable.ic_announcements,
            R.drawable.ic_maps,
            R.drawable.ic_help,
            R.drawable.ic_missing_persons,
            R.drawable.ic_disaster_mode,
    };
    private final String[] slide_headings = {

            "Announcements",
            "Maps",
            "Help Requests",
            "Missing Persons",
            "Disaster Mode"
    };

    private final String[] slide_desc = {

            "View important announcements and notifications",
            "View relief camp locations and affected areas",
            "Register help requests asking for supplies",
            "Report and search for missing persons",
            "Communicate with and locate nearby people"
    };

    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view ==  o;
    }

    @NonNull
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = view.findViewById(R.id.slide_image);
        TextView slideHeading = view.findViewById(R.id.slide_heading);
        TextView slideDescription = view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_desc[position]);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem (@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((RelativeLayout)object);
    }

}
