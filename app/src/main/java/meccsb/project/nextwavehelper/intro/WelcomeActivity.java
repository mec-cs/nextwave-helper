package meccsb.project.nextwavehelper.intro;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.HapticFeedbackConstants;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.MainActivity;
import meccsb.project.nextwavehelper.main.SharedPref;
import meccsb.project.nextwavehelper.main.TextValidator;

public class WelcomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        final SharedPref sharedPref = new SharedPref(this);

        if(sharedPref.getBoolean("isFirstRun", true)) {
            startActivity(new Intent(WelcomeActivity.this, IntroActivity.class));
            WelcomeActivity.this.finish();
        }

        final TextView titleText = findViewById(R.id.welcome_form_title);
        final TextView instructionText = findViewById(R.id.welcome_form_instruction);
        final Button submitBtn = findViewById(R.id.welcome_form_submit_btn);
        final EditText nameField = findViewById(R.id.welcome_form_name);
        final EditText phoneField = findViewById(R.id.welcome_form_phone);

        // If we are editing the profile then we are reusing this activity for editing.
        if (sharedPref.getBoolean("isEditingProfile", false)) {
            titleText.setText("Edit Profile");
            instructionText.setVisibility(View.INVISIBLE);
            nameField.setText(sharedPref.getString("userName", null));
            phoneField.setText(sharedPref.getString("userPhone", null));
            submitBtn.setText("Finish");
        }

        nameField.addTextChangedListener(
                new TextValidator(nameField,"^[A-Za-z \\.]+$", "name"));
        phoneField.addTextChangedListener(
                new TextValidator(phoneField, "^[0-9]{10}$", "phone number"));
        submitBtn.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (checkError(nameField) && checkError(phoneField)) {
                sharedPref.setString("userName", nameField.getText().toString());
                sharedPref.setString("userPhone", phoneField.getText().toString());
                sharedPref.setBoolean("isWelcomeFormFilled", true);
                openMainActivity();
            }
        });
    }

    private Boolean checkError(EditText editText){
        String test = String.valueOf(editText.getText());
        if(TextUtils.isEmpty(test)){
            editText.setError("This field cannot be empty!");
            return false;
        }
        final CharSequence error = editText.getError();
        return error == null;
    }

    private void openMainActivity(){
        Intent intent =  new Intent(this, MainActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        finish();
    }
}
