package meccsb.project.nextwavehelper.main;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;

import com.google.android.material.navigation.NavigationView;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.listener.multi.BaseMultiplePermissionsListener;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.announcements.AnnouncementsFragment;
import meccsb.project.nextwavehelper.disastermode.DisasterFragment;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsFragment;
import meccsb.project.nextwavehelper.maps.MapsFragment;
import meccsb.project.nextwavehelper.missingpersons.MissingPersonsFragment;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;
import meccsb.project.nextwavehelper.roomdb.viewmodels.AnnouncementViewModel;

public class HomeFragment extends Fragment implements View.OnClickListener{
    /*
     * Step 1: Here, we need to inject the ViewModelFactory.
     * */
    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        FragmentManager fragmentManager = getFragmentManager();
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);

        CardView announcementsCard = view.findViewById(R.id.card_announcements);
        CardView mapsCard = view.findViewById(R.id.card_maps);
        CardView helpRequestsCard = view.findViewById(R.id.card_help_requests);
        CardView missingPersonsCard = view.findViewById(R.id.card_missing_persons);
        CardView disasterModeCard = view.findViewById(R.id.card_disaster_mode);

        /* To set the latest announcement*/
        final TextView latestAnnouncementTime = view.findViewById(R.id.last_announcement_date);
        final TextView latestAnnouncementText = view.findViewById(R.id.last_announcement_text);
        AnnouncementViewModel announcementViewModel = ViewModelProviders.of(this, viewModelFactory).get(AnnouncementViewModel.class);
        announcementViewModel.getLatestAnnouncement().observe(this, new Observer<Announcement>() {
            @Override
            public void onChanged(Announcement announcement) {
                if(announcement != null) {
                    latestAnnouncementTime.setText(announcement.getTime());
                    latestAnnouncementText.setText(Html.fromHtml(announcement.getDescription(), Html.FROM_HTML_MODE_LEGACY));
                }
            }
        });

        /* Set title text according to the fragment*/
        getActivity().setTitle(R.string.app_name);

        /* Set navigation view item as checked */
        navigationView.setCheckedItem(R.id.nav_home);

        /* Clear BackStack*/
        Objects.requireNonNull(fragmentManager).popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        /* Set on click listeners to the cards */
        announcementsCard.setOnClickListener(this);
        mapsCard.setOnClickListener(this);
        helpRequestsCard.setOnClickListener(this);
        missingPersonsCard.setOnClickListener(this);
        disasterModeCard.setOnClickListener(this);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.menu_search);
        item.setVisible(false);
    }

    @Override
    public void onStart() {
        super.onStart();
        /* Get permissions */
        this.getPermissions();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void onClick(View v) {
        Fragment fragment;
        switch (v.getId()){
            case R.id.card_announcements:
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                fragment = new AnnouncementsFragment();
                replaceFragments(fragment);
                break;
            case R.id.card_maps:
                fragment = new MapsFragment();
                replaceFragments(fragment);
                break;
            case R.id.card_help_requests:
                fragment = new HelpRequestsFragment();
                replaceFragments(fragment);
                break;
            case R.id.card_missing_persons:
                v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                fragment = new MissingPersonsFragment();
                replaceFragments(fragment);
                break;
            case R.id.card_disaster_mode:
                fragment = new DisasterFragment();
                replaceFragments(fragment);
                break;
        }

    }

    private void replaceFragments(Fragment newFragment){
        newFragment.setEnterTransition(new Fade(Fade.IN));
        FragmentTransaction fragmentTransaction = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, newFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void getPermissions() {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(new BaseMultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (!report.areAllPermissionsGranted()) {
                            showSettingsDialog();
                        }
                    }
                })
                .onSameThread()
                .check();
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Need Permissions");
        builder.setMessage("Some of the permissions were denied without which the application won't work. " +
                "Please grant them in app settings");
        builder.setPositiveButton("Goto Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Close App", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                Objects.requireNonNull(getActivity()).moveTaskToBack(true);
                getActivity().finish();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", Objects.requireNonNull(getActivity()).getPackageName(), null);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setData(uri);
        startActivity(intent);
    }
}
