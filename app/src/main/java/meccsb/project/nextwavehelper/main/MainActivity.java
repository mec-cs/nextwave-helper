package meccsb.project.nextwavehelper.main;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SwitchCompat;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.transition.Fade;

import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.announcements.AnnouncementsFragment;
import meccsb.project.nextwavehelper.api.RescueWorker;
import meccsb.project.nextwavehelper.api.RescueWorkerWebservice;
import meccsb.project.nextwavehelper.disastermode.DisasterFragment;
import meccsb.project.nextwavehelper.helprequests.HelpRequestsFragment;
import meccsb.project.nextwavehelper.intro.WelcomeActivity;
import meccsb.project.nextwavehelper.maps.MapsFragment;
import meccsb.project.nextwavehelper.missingpersons.MissingPersonsFragment;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector, NavigationView.OnNavigationItemSelectedListener, SwitchCompat.OnCheckedChangeListener {
    private DrawerLayout drawer;
    private SharedPref sharedPref;
    @Inject
    RescueWorkerWebservice rescueWorkerWebservice;
    /*
     * Step 1: Rather than injecting the ViewModelFactory
     * in the activity, we are going to implement the
     * HasActivityInjector and inject the ViewModelFactory
     * into our UserProfileFragment.
     * */
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject
    LocationListener locationListener;
    private RescueWorker currentUser;

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    private Runnable locationTransmitterTask = () -> {
        // Call RescueWorker API Service.
        rescueWorkerWebservice.sendUpdate(currentUser).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                Log.d("MainActivity","LocationUpdate Success!");
            }

            @Override
            public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                Log.d("MainActivity","LocationUpdate Failed!");
            }
        });
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar);
        sharedPref = new SharedPref(this);
        if(!sharedPref.getBoolean("isWelcomeFormFilled", false)) {
            startActivity(new Intent(MainActivity.this, WelcomeActivity.class));
            MainActivity.this.finish();
        }

        /* Saved night mode state */
        if (sharedPref.getBoolean("NightMode", false)) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        this.configureDagger();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* To get toolbar displayed */
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /* For night mode switch*/
        View headerView = navigationView.getHeaderView(0);
        SwitchCompat nightModeSwitch = headerView.findViewById(R.id.nightMode_switch);
        if (sharedPref.getBoolean("NightMode", false)) {
            nightModeSwitch.setChecked(true);
        }
        nightModeSwitch.setOnCheckedChangeListener(this);

        /* Set the user info in the drawer */
        TextView userNameText = headerView.findViewById(R.id.header_userName);
        TextView userPhoneText = headerView.findViewById(R.id.header_userPhone);
        if(sharedPref.getBoolean("isWelcomeFormFilled", false)){
            currentUser = new RescueWorker(
                    sharedPref.getString("userName", null),
                    sharedPref.getString("userPhone", null));
            userNameText.setText(currentUser.getWorkerName());
            userPhoneText.setText(currentUser.getWorkerPhone());
        }

        /* Get the hamburger icon with animation */
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        /* Start home fragment by default */
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
            navigationView.setCheckedItem(R.id.nav_home);
        }

        /* init the location transmitter if the user is a rescue worker */
        if (sharedPref.getBoolean("isRescueWorker", false)) {
            initLocationUpdater();
            ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
            exec.scheduleAtFixedRate(locationTransmitterTask, 10, 300, TimeUnit.SECONDS);
        }
    }

    private void initLocationUpdater() {
        if (!locationListener.hasActiveObservers()) {
            locationListener.observe(this, location -> {
                currentUser.setLatitude(location.getLatitude());
                currentUser.setLongitude(location.getLongitude());
            });
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        /* Switch between the clicked item */
        switch (menuItem.getItemId()) {
            case R.id.nav_home:
                replaceFragments(new HomeFragment());
                break;
            case R.id.nav_announcements:
                replaceFragments(new AnnouncementsFragment());
                break;
            case R.id.nav_maps:
                replaceFragments(new MapsFragment());
                break;
            case R.id.nav_help:
                replaceFragments(new HelpRequestsFragment());
                break;
            case R.id.nav_missing_persons:
                replaceFragments(new MissingPersonsFragment());
                break;
            case R.id.nav_disaster:
                replaceFragments(new DisasterFragment());
                break;
            case R.id.nav_settings:
                replaceFragments(new SettingsFragment());
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        /* We don't want back button to close the app if the navigation drawer is open */
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    private void replaceFragments(Fragment fragment) {
        fragment.setEnterTransition(new Fade(Fade.IN));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
            sharedPref.setBoolean("NightMode", true);
        } else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
            sharedPref.setBoolean("NightMode", false);
        }
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();
    }

    private void configureDagger(){
        /*
         * Step 2: We still need to inject this method
         * into our activity so that our fragment can
         * inject the ViewModelFactory
         * */
        AndroidInjection.inject(this);
    }
}
