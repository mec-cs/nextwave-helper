package meccsb.project.nextwavehelper.main;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.google.android.material.navigation.NavigationView;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.api.RescueWorkerWebservice;
import meccsb.project.nextwavehelper.intro.IntroActivity;
import meccsb.project.nextwavehelper.intro.WelcomeActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsFragment extends PreferenceFragmentCompat {
    private SharedPref sharedPref;
    private Activity mainActivity;

    @Inject
    RescueWorkerWebservice rescueWorkerWebservice;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mainActivity = getActivity();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.app_preferences, rootKey);
        sharedPref = new SharedPref(App.context);
        Preference volunteerPref = findPreference("rescueWorkerMode");
        if (sharedPref.getBoolean("isRescueWorker", false)) {
            volunteerPref.setTitle("Stop volunteering as rescue worker");
        } else {
            volunteerPref.setTitle("Volunteer as rescue worker");
        }
    }

    @Override
    public boolean onPreferenceTreeClick(Preference preference) {
        switch (preference.getKey()) {
            case "editProfile":
                sharedPref.setBoolean("isEditingProfile", true);
                startActivity(new Intent(mainActivity, WelcomeActivity.class));
                mainActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                mainActivity.finish();
                break;
            case "runIntro":
                startActivity(new Intent(mainActivity, IntroActivity.class));
                mainActivity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                mainActivity.finish();
                break;
            case "resetApp":
                showResetAppDialog();
                break;
            case "rescueWorkerMode":
                if (sharedPref.getBoolean("isRescueWorker", false)) {
                    showOptOutDialog();
                } else {
                    showOptInDialog();
                }
                break;
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        /* Set navigation view item as checked */
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_settings);

        /* Set title text according to the fragment*/
        getActivity().setTitle("Settings");

        setHasOptionsMenu(true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.menu_search);
        item.setVisible(false);
    }
    private void showOptInDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Opt in as Rescue Worker");
        builder.setMessage("This is permanent. Don't do this unless you're in a safe and stable position.");
        builder.setPositiveButton("Opt in", (dialog, which) -> {
            dialog.cancel();
            sharedPref.setBoolean("isRescueWorker", true);
            openMainActivity();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.setCancelable(false);
        builder.show();
    }

    private void showOptOutDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Opt out as Rescue Worker");
        builder.setMessage("Do you want to opt out as a Rescue worker?");
        builder.setPositiveButton("Opt out", (dialog, which) -> {
            dialog.cancel();
            sharedPref.setBoolean("isRescueWorker", false);
            openMainActivity();
            rescueWorkerWebservice.deleteWorker(sharedPref.getString("userPhone", null)).enqueue(new Callback<Void>() {
                @Override
                public void onResponse(@NotNull Call<Void> call, @NotNull Response<Void> response) {
                    Log.d("SettingsFragment", "Opt Out Success!");
                }

                @Override
                public void onFailure(@NotNull Call<Void> call, @NotNull Throwable t) {
                    Log.d("SettingsFragment", "Opt Out Failed!");
                }
            });
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.setCancelable(false);
        builder.show();
    }

    private void showResetAppDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getContext()));
        builder.setTitle("Are you sure?");
        builder.setMessage("This will clear all the cached data and settings. The app will force close upon resetting.");
        builder.setPositiveButton("Ok", (dialog, which) -> {
            dialog.cancel();
            ((ActivityManager) App.context.getSystemService(Context.ACTIVITY_SERVICE)).clearApplicationUserData();
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());
        builder.setCancelable(false);
        builder.show();
    }

    private void openMainActivity() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        Objects.requireNonNull(getActivity()).finish();
    }
}
