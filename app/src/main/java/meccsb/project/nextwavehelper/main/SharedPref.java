package meccsb.project.nextwavehelper.main;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {
    private final SharedPreferences mySharedPref;
    public SharedPref(Context context){
        mySharedPref = context.getSharedPreferences("preferences", Context.MODE_PRIVATE);
    }

    public void setBoolean(String key, Boolean value){
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public Boolean getBoolean(String key,Boolean defValue){
        return mySharedPref.getBoolean(key, defValue);
    }

    public void setString(String key, String value){
        SharedPreferences.Editor editor = mySharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key, String defValue){
        return mySharedPref.getString(key, defValue);
    }
}
