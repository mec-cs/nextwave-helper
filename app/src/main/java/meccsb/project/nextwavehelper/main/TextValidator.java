package meccsb.project.nextwavehelper.main;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextValidator implements TextWatcher {
    private final TextView textView;
    private final String pattern;
    private final String fieldType;

    public TextValidator(TextView textView, String pattern, String fieldType) {
        this.textView = textView;
        this.pattern = pattern;
        this.fieldType = fieldType;
    }

    private void validate(TextView textView, String text) {
        final String phonePattern = pattern;
        Pattern pattern = Pattern.compile(phonePattern);
        Matcher matcher = pattern.matcher(text);
        if(matcher.matches())
            textView.setError(null);
        else
            textView.setError("Please enter a valid " + fieldType);
    }

    @Override
    final public void afterTextChanged(Editable s) {
        String text = textView.getText().toString();
        validate(textView, text);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {/* Don't Care*/}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {/* Don't Care*/}
}
