package meccsb.project.nextwavehelper.maps;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProvider;
import androidx.transition.Fade;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.ui.PlaceAutocompleteFragment;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.ui.PlaceSelectionListener;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.mapboxsdk.style.sources.VectorSource;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.BuildConfig;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.LocationListener;

public class AffectedLocationsFragment extends Fragment {
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocationListener locationListener;

    private static final String TAG_FRAGMENT_SEARCH = "fragment_search";
    private static final String TAG_FRAGMENT_AFFECTED_MAP = "fragment_affected_map";

    private RequestQueue volleyRequestQueue = Volley.newRequestQueue(App.context);
    private FeatureCollection lineData;

    private MapView mapView;
    private MapboxMap myMapboxMap;
    private LatLng currentLatLng;
    private String mapStyle;
    private Boolean editFlag = false;
    private ProgressBar loadingProgressBar;

    private PlaceAutocompleteFragment autocompleteFragment = PlaceAutocompleteFragment.newInstance(BuildConfig.MapBoxToken);
    private Fragment currentFragment;
    private FragmentManager fragmentManager;

    static AffectedLocationsFragment newInstance() {
        AffectedLocationsFragment f = new AffectedLocationsFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(App.context, BuildConfig.MapBoxToken);
        fragmentManager = getFragmentManager();
        if (fragmentManager != null) {
            currentFragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_AFFECTED_MAP);
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_affected_location, container, false);
        loadingProgressBar = view.findViewById(R.id.affected_locations_loading_progressBar);
        int searchColor;
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            mapStyle = Style.DARK;
            searchColor = Color.BLACK;
        } else {
            mapStyle = Style.MAPBOX_STREETS;
            searchColor = Color.WHITE;
        }

        // Limit search results to India
        PlaceOptions placeOptions = PlaceOptions.builder()
                .country("IN")
                .backgroundColor(searchColor)
                .toolbarColor(searchColor)
                .build();
        autocompleteFragment = PlaceAutocompleteFragment.newInstance(BuildConfig.MapBoxToken, placeOptions);

        // MapView
        mapView = view.findViewById(R.id.affected_locations_mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(mapboxMap -> {
            myMapboxMap = mapboxMap;
            myMapboxMap.setStyle(mapStyle, style -> {
                enableLocationComponent(style);
                initLocationFAB(view);
                initSearchFragment();
                initSearchFAB(view);
                initAffectedAreas(view);
            });
        });


        // disable search bar
        setHasOptionsMenu(false);

        return view;
    }

    private void viewSearchResult(BoundingBox boundingBox) {
        LatLng northEast, southWest;
        northEast = new LatLng(boundingBox.northeast().latitude(), boundingBox.northeast().longitude());
        southWest = new LatLng(boundingBox.southwest().latitude(), boundingBox.southwest().longitude());
        LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(northEast) // Northeast
                .include(southWest) // Southwest
                .build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50), 3000);
    }

    @SuppressWarnings("MissingPermission")
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Get an instance of the component
        LocationComponent locationComponent = myMapboxMap.getLocationComponent();
        // Activate with options
        locationComponent.activateLocationComponent(new LocationComponentActivationOptions
                .Builder(App.context, loadedMapStyle)
                .build());
        // Enable to make component visible
        locationComponent.setLocationComponentEnabled(true);
        // Set the component's render mode
        locationComponent.setRenderMode(RenderMode.COMPASS);
    }

    private void initLocationFAB(final View view) {
        FloatingActionButton locationFAB = view.findViewById(R.id.affected_locations_location_fab);
        locationFAB.setVisibility(View.VISIBLE);
        locationListener.observe(getViewLifecycleOwner(), location -> {
            Log.d("Location", String.format("Lng: %f, Lat: %f", location.getLongitude(), location.getLatitude()));
            myMapboxMap.getLocationComponent().forceLocationUpdate(location);
            currentLatLng = new LatLng(location);
        });
        locationFAB.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (currentLatLng != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatLng)).zoom(15).build();
                myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
            } else {
                Snackbar.make(view, "Cannot get the current GPS coordinates", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void viewLatLng(double lat, double lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(15).build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
    }

    private void initSearchFragment() {
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            Fragment fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_AFFECTED_MAP);

            @Override
            public void onPlaceSelected(CarmenFeature carmenFeature) {
                Point point = carmenFeature.center();
                if (carmenFeature.bbox() != null) {
                    viewSearchResult(Objects.requireNonNull(carmenFeature.bbox()));
                } else if (point != null) {
                    viewLatLng(point.latitude(), point.longitude());
                }
                // hide the search fragment and show the map.
                showFragment(fragment, TAG_FRAGMENT_AFFECTED_MAP);
            }

            @Override
            public void onCancel() {
                showFragment(fragment, TAG_FRAGMENT_AFFECTED_MAP);
            }
        });
    }

    private void initSearchFAB(View view) {
        FloatingActionButton searchFAB = view.findViewById(R.id.affected_locations_search_fab);
        searchFAB.setVisibility(View.VISIBLE);
        searchFAB.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            Fragment fragment;
            fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_SEARCH);
            if (fragment == null) {
                fragment = autocompleteFragment;
            }
            showFragment(fragment, TAG_FRAGMENT_SEARCH);
        });
    }

    private MapboxMap.OnMapClickListener roadClickListener = new MapboxMap.OnMapClickListener() {
        @Override
        public boolean onMapClick(@NonNull LatLng point) {
            Vibrator vibrator = (Vibrator) Objects.requireNonNull(getActivity()).getSystemService(Context.VIBRATOR_SERVICE);
            final PointF pixel = myMapboxMap.getProjection().toScreenLocation(point);
            RectF rectF = new RectF(pixel.x - 10, pixel.y - 10, pixel.x + 10, pixel.y + 10);
            List<Feature> features = myMapboxMap.queryRenderedFeatures(rectF, "marked_roads");
            if (features.size() > 0) {
                vibrator.vibrate(25);
                Feature feature = features.get(0);
                Toast.makeText(App.context, "Deleting selected feature", Toast.LENGTH_SHORT).show();
                loadingProgressBar.setVisibility(View.VISIBLE);
                deleteRoad(feature);
                return true;
            } else {
                features = myMapboxMap.queryRenderedFeatures(rectF, "road");
                if (features.size() > 0) {
                    vibrator.vibrate(25);
                    Feature feature = features.get(0);
                    Toast.makeText(App.context, "Adding selected feature", Toast.LENGTH_SHORT).show();
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    addRoad(feature);
                    return true;
                } else {
                    return false;
                }
            }
        }
    };

    private void initAffectedAreas(final View view) {
        final FloatingActionButton editFAB = view.findViewById(R.id.affected_locations_edit_fab);
        editFAB.setVisibility(View.VISIBLE);
        editFAB.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (editFlag) {
                myMapboxMap.setLatLngBoundsForCameraTarget(null);
                myMapboxMap.setMinZoomPreference(0);
                editFAB.setImageResource(R.drawable.ic_edit_mode);
                myMapboxMap.removeOnMapClickListener(roadClickListener);
                editFlag = false;
            } else {
                if (currentLatLng != null) {
                    // disable interaction
                    myMapboxMap.getUiSettings().setAllGesturesEnabled(false);
                    CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatLng)).zoom(16).build();
                    myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), new MapboxMap.CancelableCallback() {
                        @Override
                        public void onCancel() {
                        }

                        @Override
                        public void onFinish() {
                            // re-enable interactions after zooming is finished.
                            myMapboxMap.getUiSettings().setAllGesturesEnabled(true);
                            myMapboxMap.resetNorth();
                            LatLngBounds nearCurrLocBounds = myMapboxMap.getProjection().getVisibleRegion(true).latLngBounds;
                            Log.d("Bounds", nearCurrLocBounds.toString());
                            myMapboxMap.setLatLngBoundsForCameraTarget(nearCurrLocBounds);
                            myMapboxMap.setMinZoomPreference(16);
                            myMapboxMap.addOnMapClickListener(roadClickListener);
                            editFAB.setImageResource(R.drawable.ic_close);
                            editFlag = true;
                            Snackbar.make(view, "The map movement will be restricted in edit mode", Snackbar.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    Snackbar.make(view, "You cannot edit affected locations without GPS", Snackbar.LENGTH_SHORT).show();
                }
            }

        });
        Style style = myMapboxMap.getStyle();
        if (style != null) {
            style.addSource(new VectorSource("mapbox-streets", "mapbox://mapbox.mapbox-streets-v8"));
            style.addLayer(new LineLayer("road", "mapbox-streets")
                    .withSourceLayer("road")
                    .withProperties(PropertyFactory.lineWidth(3f),
                            PropertyFactory.lineOpacity(0.1f),
                            PropertyFactory.lineColor(Color.WHITE)));
        }
        refreshRoadLayer();
    }

    private void refreshRoadLayer(){
        URL geoJsonURL = null;
        try {
            geoJsonURL = new URL("https://nextwavehelper.cf/affected_locations/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        if (myMapboxMap != null) {
            Style style = myMapboxMap.getStyle();
            if (style != null) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                style.removeLayer("marked_roads");
                style.removeSource("roads_source");
                if (geoJsonURL != null) {
                    style.addSource(new GeoJsonSource("roads_source", geoJsonURL));
                }
                style.addLayer(new LineLayer("marked_roads", "roads_source")
                        .withProperties(PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
                                PropertyFactory.lineJoin(Property.LINE_JOIN_MITER),
                                PropertyFactory.lineOpacity(.7f),
                                PropertyFactory.lineWidth(3f),
                                PropertyFactory.lineColor(Color.RED)));
                loadingProgressBar.setVisibility(View.INVISIBLE);
            }
        }
    }

    private void addRoad(final Feature feature){
        final String PROTOCOL_CHARSET = "utf-8";
        final String payload = feature.toJson();

        String featureURL = String.format(
                "https://nextwavehelper.cf/api/affected_location/add/%s",
                feature.id());

        StringRequest addRoadRequest = new StringRequest(Request.Method.POST, featureURL,
                response -> refreshRoadLayer(),
                error -> {
                    Toast.makeText(App.context, "Couldn't Add The Clicked Feature", Toast.LENGTH_SHORT).show();
                    refreshRoadLayer();
                }
        ){
            /* Passing some request headers */
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("api-key", BuildConfig.WebAPIKey);
                return headers;
            }

            @Override
            public byte[] getBody() {
                try {
                    return payload.getBytes(PROTOCOL_CHARSET);
                } catch (UnsupportedEncodingException e) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", payload, PROTOCOL_CHARSET);
                    return null;
                }
            }
        };
        volleyRequestQueue.add(addRoadRequest);
    }

    private void deleteRoad(final Feature feature) {
        String featureURL = String.format(
                "https://nextwavehelper.cf/api/affected_location/delete/%s",
                feature.id());

        StringRequest deleteRequest = new StringRequest(Request.Method.POST, featureURL,
                response -> refreshRoadLayer(),
                error -> {
                    Toast.makeText(App.context, "Couldn't Delete The Clicked Feature", Toast.LENGTH_SHORT).show();
                    refreshRoadLayer();
                }
        ) {
            /* Passing some request headers */
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("api-key", BuildConfig.WebAPIKey);
                return headers;
            }
        };
        volleyRequestQueue.add(deleteRequest);
    }

    private void showFragment(Fragment fragment, String tag) {
        if (!fragment.equals(currentFragment)) {
            fragment.setEnterTransition(new Fade(Fade.IN));
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment.isAdded()) {
                ft.show(fragment);
            } else {
                ft.add(R.id.maps_fragments_container, fragment, tag);
            }
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            currentFragment = fragment;
            ft.commit();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
