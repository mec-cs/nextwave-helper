package meccsb.project.nextwavehelper.maps;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.transition.Fade;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.util.Objects;

import meccsb.project.nextwavehelper.R;

public class MapsFragment extends Fragment implements ReliefCampListFragment.OnCampSelected, ReliefCampListMapFragment.OnMarkerSelected {

    private static final String TAG_FRAGMENT_LIST = "fragment_list";
    private static final String TAG_FRAGMENT_MAP = "fragment_map";
    private static final String TAG_FRAGMENT_AFFECTED_MAP = "fragment_affected_map";

    private FragmentManager fragmentManager;
    private Fragment currentFragment;
    private BottomNavigationView bottomNavigationView;

    private AffectedLocationsFragment affectedLocationsFragment = AffectedLocationsFragment.newInstance();
    private ReliefCampListFragment reliefCampListFragment = ReliefCampListFragment.newInstance();
    private ReliefCampListMapFragment reliefCampListMapFragment = ReliefCampListMapFragment.newInstance(-1);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);

        hideFragment(TAG_FRAGMENT_LIST);
        hideFragment(TAG_FRAGMENT_AFFECTED_MAP);
        hideFragment(TAG_FRAGMENT_MAP);

        bottomNavigationView = view.findViewById(R.id.maps_bottom_nav);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.maps_bottom_nav_relief_camps_list);
        /* Set title text according to the fragment*/
        getActivity().setTitle("Relief Camps List");
        /* Set navigation view item as checked */
        navigationView.setCheckedItem(R.id.nav_maps);
        return view;
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Activity activity = getActivity();
            assert activity != null;
            Fragment fragment, mapFragment;
            Vibrator vibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(50);
            switch (item.getItemId()) {
                case R.id.maps_bottom_nav_affected_locations:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_AFFECTED_MAP);
                    if (fragment == null) {
                        fragment = affectedLocationsFragment;
                    }

                    // remove the relief camps maps fragment if exists and create a new instance.
                    mapFragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_MAP);
                    if (mapFragment != null) {
                        fragmentManager.beginTransaction()
                                .remove(mapFragment).commit();
                        reliefCampListMapFragment = ReliefCampListMapFragment.newInstance(-1);
                    }

                    showFragment(fragment, TAG_FRAGMENT_AFFECTED_MAP);
                    activity.setTitle("Affected Locations");
                    return true;

                case R.id.maps_bottom_nav_relief_camps_list:
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_LIST);
                    if (fragment == null) {
                        fragment = reliefCampListFragment;
                    }
                    showFragment(fragment, TAG_FRAGMENT_LIST);
                    activity.setTitle("Relief Camps List");
                    return true;

                case R.id.maps_bottom_nav_relief_camps_map:
                    reliefCampListFragment.setSearchQuery("");
                    fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_MAP);
                    if (fragment == null) {
                        fragment = reliefCampListMapFragment;
                    }

                    // remove the affected location maps fragment if exists and create a new instance.
                    mapFragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_AFFECTED_MAP);
                    if (mapFragment != null) {
                        fragmentManager.beginTransaction().remove(mapFragment).commit();
                        affectedLocationsFragment = AffectedLocationsFragment.newInstance();
                    }

                    showFragment(fragment, TAG_FRAGMENT_MAP);
                    activity.setTitle("Relief Camps Map");
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getChildFragmentManager();
    }

    private void showFragment(Fragment fragment, String tag) {
        if(!fragment.equals(currentFragment)) {
            fragment.setEnterTransition(new Fade(Fade.IN));
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment.isAdded()) {
                ft.show(fragment);
            } else {
                ft.add(R.id.maps_fragments_container, fragment, tag);
            }
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            currentFragment = fragment;
            ft.commit();
        }
    }

    private void hideFragment(String tag) {
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.hide(fragment);
            ft.commit();
        }
    }

    @Override
    public void selectCamp(int campID) {
        if(fragmentManager.findFragmentByTag(TAG_FRAGMENT_MAP) != null){
            reliefCampListMapFragment.setSelectedCampID(campID);
        } else {
            reliefCampListMapFragment = ReliefCampListMapFragment.newInstance(campID);
        }
        bottomNavigationView.setSelectedItemId(R.id.maps_bottom_nav_relief_camps_map);
    }

    @Override
    public void selectMarker(String campName) {
        reliefCampListFragment.setSearchQuery(campName);
        bottomNavigationView.setSelectedItemId(R.id.maps_bottom_nav_relief_camps_list);
    }
}
