package meccsb.project.nextwavehelper.maps;

import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;

public class ReliefCampAdapter extends ListAdapter<ReliefCamp, ReliefCampAdapter.ReliefCampHolder> {
    private OnItemClickListener listener;

    ReliefCampAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<ReliefCamp> DIFF_CALLBACK = new DiffUtil.ItemCallback<ReliefCamp>() {
        @Override
        public boolean areItemsTheSame(@NonNull ReliefCamp oldItem, @NonNull ReliefCamp newItem) {
            return oldItem.getCampID() == newItem.getCampID();
        }

        @Override
        public boolean areContentsTheSame(@NonNull ReliefCamp oldItem, @NonNull ReliefCamp newItem) {
            return oldItem.getAddress().equals(newItem.getAddress()) &&
                    oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getPhone().equals(newItem.getPhone()) &&
                    oldItem.getLatitude().equals(newItem.getLatitude()) &&
                    oldItem.getDistanceToCurrLoc().equals(newItem.getDistanceToCurrLoc()) &&
                    oldItem.getLongitude().equals(newItem.getLongitude());
        }
    };
    @NonNull
    @Override
    public ReliefCampHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.relief_camp_item, parent, false);
        return new ReliefCampHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ReliefCampHolder holder, int position) {
        ReliefCamp currentReliefCamp = getItem(position);
        holder.nameText.setText(currentReliefCamp.getName());
        holder.addressText.setText(String.format("Address: %s", currentReliefCamp.getAddress()));
        holder.phoneText.setText(String.format("Phone: %s", currentReliefCamp.getPhone()));
        if (currentReliefCamp.getDistanceToCurrLoc().equals(-1f)) {
            holder.distanceText.setVisibility(View.INVISIBLE);
            holder.distanceLabel.setVisibility(View.INVISIBLE);
        } else
            holder.distanceText.setText(String.format("%.2f KMs", currentReliefCamp.getDistanceToCurrLoc()));
    }

    class ReliefCampHolder extends RecyclerView.ViewHolder{
        private final TextView nameText;
        private final TextView phoneText;
        private final TextView addressText;
        private final TextView distanceLabel;
        private final TextView distanceText;

        ReliefCampHolder(@NonNull View itemView) {
            super(itemView);
            nameText = itemView.findViewById(R.id.relief_camp_item_name);
            phoneText = itemView.findViewById(R.id.relief_camp_item_phone);
            addressText = itemView.findViewById(R.id.relief_camp_item_address);
            distanceLabel = itemView.findViewById(R.id.relief_camp_item_distance_label);
            distanceText = itemView.findViewById(R.id.relief_camp_item_distance);

            // Set on click for the items
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
                    int position = getAdapterPosition();
                    if (listener != null && position != RecyclerView.NO_POSITION) {
                        listener.onItemClick(getItem(position));
                    }
                }
            });
        }
    }

    // interface to implement by our fragment/activity
    public interface OnItemClickListener {
        void onItemClick(ReliefCamp reliefCamp);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }
}
