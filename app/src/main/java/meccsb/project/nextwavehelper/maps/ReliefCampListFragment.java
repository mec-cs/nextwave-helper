package meccsb.project.nextwavehelper.maps;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.android.material.navigation.NavigationView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.LocationListener;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;
import meccsb.project.nextwavehelper.roomdb.viewmodels.ReliefCampViewModel;

public class ReliefCampListFragment extends Fragment {
    /*
     * Step 1: Here, we need to inject the ViewModelFactory.
     * */
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocationListener locationListener;

    private final ReliefCampAdapter adapter = new ReliefCampAdapter();
    private ReliefCampViewModel reliefCampViewModel;
    private OnCampSelected onCampSelectedListener;
    private boolean refresh = false;
    private SearchView searchView;

    static ReliefCampListFragment newInstance() {
        ReliefCampListFragment f = new ReliefCampListFragment();
        Bundle args = new Bundle();
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reliefCampViewModel = ViewModelProviders.of(this, viewModelFactory).get(ReliefCampViewModel.class);
        reliefCampViewModel.setCurrentLocation(new Location(""));
        // Get location updates from location service.
        locationListener.observe(this, new Observer<Location>() {
            @Override
            public void onChanged(Location location) {
                Log.d("ReliefCampListFragment", String.format("Lng: %f, Lat: %f", location.getLongitude(), location.getLatitude()));
                // Notify the view model to recalculate the distances.
                if(!refresh) {
                    reliefCampViewModel.setCurrentLocation(location);
                    refreshView();
                }
            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
        onCampSelectedListener = (OnCampSelected) getParentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_relief_camp_list, container, false);
        final NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);
        final RecyclerView recyclerView = view.findViewById(R.id.relief_camp_recycler_view);
        final ProgressBar progressBar = view.findViewById(R.id.reliefCamp_progressBar);
        final LiveData<List<ReliefCamp>> reliefCamps = reliefCampViewModel.getAllReliefCamps();

        /* To get the search button in menu*/
        setHasOptionsMenu(true);

        /* Set navigation view item as checked */
        navigationView.setCheckedItem(R.id.nav_maps);

        reliefCamps.observe(this, new Observer<List<ReliefCamp>>() {
            @Override
            public void onChanged(List<ReliefCamp> reliefCamps) {
                adapter.submitList(reliefCamps);
                if(reliefCamps.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        adapter.setOnItemClickListener(new ReliefCampAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ReliefCamp reliefCamp) {
                Log.d("Click", "Clicked: " + reliefCamp.getName());
                onCampSelectedListener.selectCamp(reliefCamp.getCampID());
            }
        });
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setQueryHint("Search Relief Camps");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                reliefCampViewModel.setQuery(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                reliefCampViewModel.setQuery(newText);
                return true;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }

    private void refreshView(){
        Objects.requireNonNull(getFragmentManager())
                .beginTransaction()
                .detach(ReliefCampListFragment.this)
                .attach(ReliefCampListFragment.this)
                .commit();
        refresh = true;
    }
    @Override
    public void onResume() {
        super.onResume();
        reliefCampViewModel.refreshFromWeb();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }

    void setSearchQuery(String campName) {
        if (searchView != null)
            searchView.setQuery(campName, false);
    }

    public interface OnCampSelected {
        void selectCamp(int campID);
    }
}
