package meccsb.project.nextwavehelper.maps;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PointF;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.transition.Fade;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.mapbox.api.geocoding.v5.models.CarmenFeature;
import com.mapbox.geojson.BoundingBox;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.location.LocationComponent;
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions;
import com.mapbox.mapboxsdk.location.modes.RenderMode;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.ui.PlaceAutocompleteFragment;
import com.mapbox.mapboxsdk.plugins.places.autocomplete.ui.PlaceSelectionListener;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.BuildConfig;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.LocationListener;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;
import meccsb.project.nextwavehelper.roomdb.viewmodels.ReliefCampViewModel;

public class ReliefCampListMapFragment extends Fragment {
    /*
     * Step 1: Here, we need to inject the ViewModelFactory.
     * */
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    @Inject
    LocationListener locationListener;

    private static final String CAMP_ID = "bundle_camp_id";
    private static final String TAG_FRAGMENT_SEARCH = "fragment_search";
    private static final String TAG_FRAGMENT_MAP = "fragment_map";

    private ReliefCampViewModel reliefCampViewModel;
    private LiveData<ReliefCamp> selectedCamp;
    private MapView mapView;
    private MapboxMap myMapboxMap;
    private LatLng currentLatLng;
    private String mapStyle;

    private PlaceAutocompleteFragment autocompleteFragment = PlaceAutocompleteFragment.newInstance(BuildConfig.MapBoxToken);
    private Fragment currentFragment = this;
    private FragmentManager fragmentManager;

    private List<Feature> markerCoordinates = new ArrayList<>();
    private OnMarkerSelected onMarkerSelectedListener;
    public static ReliefCampListMapFragment newInstance(int campID) {
        ReliefCampListMapFragment f = new ReliefCampListMapFragment();
        Bundle args = new Bundle();
        args.putInt(CAMP_ID, campID);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // MapBox init with token
        Mapbox.getInstance(App.context, BuildConfig.MapBoxToken);

        fragmentManager = getFragmentManager();

        reliefCampViewModel = ViewModelProviders.of(this, viewModelFactory).get(ReliefCampViewModel.class);
        reliefCampViewModel.setCurrentLocation(new Location(""));

        LiveData<List<ReliefCamp>> reliefCamps = reliefCampViewModel.getAllReliefCamps();
        reliefCamps.observe(this, reliefCamps1 -> {
            for (ReliefCamp camp : reliefCamps1) {
                markerCoordinates
                        .add(Feature.fromGeometry(
                                Point.fromLngLat(Double.valueOf(camp.getLongitude()), Double.valueOf(camp.getLatitude())),
                                null,
                                camp.getName()) // We set camp name as ID to search for it in camps list
                        );
            }
        });

        if (getArguments() != null && getArguments().getInt(CAMP_ID) != -1)
            selectedCamp = reliefCampViewModel.getReliefCampById(getArguments().getInt(CAMP_ID));
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
        onMarkerSelectedListener = (OnMarkerSelected) getParentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_relief_camp_map, container, false);
        int searchColor;
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            mapStyle = Style.DARK;
            searchColor = Color.BLACK;
        } else {
            mapStyle = Style.MAPBOX_STREETS;
            searchColor = Color.WHITE;
        }

        // Limit search results to India
        PlaceOptions placeOptions = PlaceOptions.builder()
                .country("IN")
                .backgroundColor(searchColor)
                .toolbarColor(searchColor)
                .build();
        autocompleteFragment = PlaceAutocompleteFragment.newInstance(BuildConfig.MapBoxToken, placeOptions);

        // MapView
        mapView = view.findViewById(R.id.relief_camp_mapView);
        mapView.getMapAsync(mapboxMap -> {
            myMapboxMap = mapboxMap;
            myMapboxMap.setStyle(mapStyle, style -> {
                enableLocationComponent(style);
                initLocationFAB(view);
                initSearchFragment();
                initSearchFAB(view);
                initSelectedCampObserver();
                initMarkersLayer(style);
            });
        });


        // disable search bar
        setHasOptionsMenu(false);

        return view;
    }

    private void viewCamp(double lat, double lng) {
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(lat, lng)).zoom(15).build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
    }

    private void viewSearchResult(BoundingBox boundingBox) {
        LatLng northEast, southWest;
        northEast = new LatLng(boundingBox.northeast().latitude(), boundingBox.northeast().longitude());
        southWest = new LatLng(boundingBox.southwest().latitude(), boundingBox.southwest().longitude());
        LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(northEast) // Northeast
                .include(southWest) // Southwest
                .build();
        myMapboxMap.animateCamera(CameraUpdateFactory.newLatLngBounds(latLngBounds, 50), 3000);
    }

    @SuppressWarnings("MissingPermission")
    private void enableLocationComponent(@NonNull Style loadedMapStyle) {
        // Get an instance of the component
        LocationComponent locationComponent = myMapboxMap.getLocationComponent();
        // Activate with options
        locationComponent.activateLocationComponent(new LocationComponentActivationOptions
                .Builder(App.context, loadedMapStyle)
                .build());
        // Enable to make component visible
        locationComponent.setLocationComponentEnabled(true);
        // Set the component's render mode
        locationComponent.setRenderMode(RenderMode.COMPASS);
    }

    private void initLocationFAB(final View view) {
        FloatingActionButton locationFAB = view.findViewById(R.id.relief_camp_map_location_fab);
        locationFAB.setVisibility(View.VISIBLE);
        locationFAB.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            locationListener.observe(getViewLifecycleOwner(), location -> {
                Log.d("Location", String.format("Lng: %f, Lat: %f", location.getLongitude(), location.getLatitude()));
                myMapboxMap.getLocationComponent().forceLocationUpdate(location);
                currentLatLng = new LatLng(location);
            });
            if (currentLatLng != null) {
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(currentLatLng)).zoom(15).build();
                myMapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000);
            } else {
                Snackbar.make(view, "Cannot get the current GPS coordinates", Snackbar.LENGTH_SHORT).show();
            }
        });
    }

    private void initSearchFragment() {
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            Fragment fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_MAP);
            @Override
            public void onPlaceSelected(CarmenFeature carmenFeature) {
                Point point = carmenFeature.center();
                if (carmenFeature.bbox() != null) {
                    viewSearchResult(Objects.requireNonNull(carmenFeature.bbox()));
                } else if (point != null) {
                    viewCamp(point.latitude(), point.longitude());
                }
                // hide the search fragment and show the map.
                showFragment(fragment, TAG_FRAGMENT_MAP);
            }

            @Override
            public void onCancel() {
                showFragment(fragment, TAG_FRAGMENT_MAP);
            }
        });
    }

    private void initSearchFAB(View view) {
        FloatingActionButton searchFAB = view.findViewById(R.id.relief_camp_map_search_fab);
        searchFAB.setVisibility(View.VISIBLE);
        searchFAB.setOnClickListener(v -> {
            Fragment fragment;
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            fragment = fragmentManager.findFragmentByTag(TAG_FRAGMENT_SEARCH);
            if (fragment == null) {
                fragment = autocompleteFragment;
            }
            showFragment(fragment, TAG_FRAGMENT_SEARCH);
        });
    }

    private void initSelectedCampObserver() {
        if (selectedCamp != null) {
            selectedCamp.observe(getViewLifecycleOwner(), reliefCamp -> {
                Toast.makeText(getContext(), "CampName: " + reliefCamp.getName(), Toast.LENGTH_SHORT).show();
                double lat = Double.valueOf(reliefCamp.getLatitude());
                double lng = Double.valueOf(reliefCamp.getLongitude());
                viewCamp(lat, lng);
            });
        }
    }

    private void initMarkersLayer(Style style) {
        // Add marker source
        style.addSource(new GeoJsonSource("marker-source",
                FeatureCollection.fromFeatures(markerCoordinates)));

        // Add the marker image to map
        style.addImage("red-marker",
                BitmapFactory.decodeResource(
                        getResources(), R.drawable.mapbox_marker_icon_default));

        SymbolLayer symbolLayer = new SymbolLayer("marker-layer", "marker-source");
        symbolLayer.withProperties(
                PropertyFactory.iconImage("red-marker")
        );
        style.addLayer(symbolLayer);
        myMapboxMap.addOnMapClickListener(point -> {
            final PointF pixel = myMapboxMap.getProjection().toScreenLocation(point);
            List<Feature> features = myMapboxMap.queryRenderedFeatures(pixel, "marker-layer");
            if (features.size() > 0) {
                String markerName = Objects.requireNonNull(features.get(0).id());
                Log.d("Tag", "CampName: " + markerName);
                onMarkerSelectedListener.selectMarker(markerName);
                return true;
            }
            return false;
        });
    }

    void setSelectedCampID(int campID) {
        Log.d("setSelectedCampID", "" + campID);
        selectedCamp = reliefCampViewModel.getReliefCampById(campID);
        if (!selectedCamp.hasObservers())
            initSelectedCampObserver();
    }

    private void showFragment(Fragment fragment, String tag) {
        if (!fragment.equals(currentFragment)) {
            fragment.setEnterTransition(new Fade(Fade.IN));
            FragmentTransaction ft = fragmentManager.beginTransaction();
            if (fragment.isAdded()) {
                ft.show(fragment);
            } else {
                ft.add(R.id.maps_fragments_container, fragment, tag);
            }
            if (currentFragment != null) {
                ft.hide(currentFragment);
            }
            currentFragment = fragment;
            ft.commit();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        reliefCampViewModel.refreshFromWeb();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapView.onCreate(savedInstanceState);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mapView.onDestroy();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }

    public interface OnMarkerSelected {
        void selectMarker(String campName);
    }
}
