package meccsb.project.nextwavehelper.missingpersons;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;

public class MissingPersonAdapter extends ListAdapter<MissingPerson, MissingPersonAdapter.MissingPersonHolder> {
    protected MissingPersonAdapter() {
        super(DIFF_CALLBACK);
    }

    private static final DiffUtil.ItemCallback<MissingPerson> DIFF_CALLBACK = new DiffUtil.ItemCallback<MissingPerson>() {
        @Override
        public boolean areItemsTheSame(@NonNull MissingPerson oldItem, @NonNull MissingPerson newItem) {
            return oldItem.getPersonID() == newItem.getPersonID();
        }

        @Override
        public boolean areContentsTheSame(@NonNull MissingPerson oldItem, @NonNull MissingPerson newItem) {
            return oldItem.getTime().equals(newItem.getTime()) &&
                    oldItem.getName().equals(newItem.getName()) &&
                    oldItem.getPhone().equals(newItem.getPhone()) &&
                    oldItem.getLocation().equals(newItem.getLocation()) &&
                    oldItem.getSex() == newItem.getSex() &&
                    oldItem.getMissing() == newItem.getMissing() &&
                    oldItem.getAge() == newItem.getAge();
        }
    };

    @NonNull
    @Override
    public MissingPersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.missing_person_item, parent, false);
        return new MissingPersonHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MissingPersonAdapter.MissingPersonHolder holder, int position) {
        MissingPerson currentMissingPerson = getItem(position);
        holder.timeText.setText(currentMissingPerson.getTime());
        holder.nameText.setText(currentMissingPerson.getName());
        holder.phoneText.setText(String.format("Phone: %s", currentMissingPerson.getPhone()));
        holder.locationText.setText(String.format("Location: %s", currentMissingPerson.getLocation()));
        switch (currentMissingPerson.getSex()) {
            case 'F':
                holder.sexText.setText("Sex: Female");
                break;
            case 'M':
                holder.sexText.setText("Sex: Male");
                break;
        }
        holder.ageText.setText(String.format("Age: %d", currentMissingPerson.getAge()));
        switch (currentMissingPerson.getMissing()) {
            case 0: // Not Missing - Green
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_maps);
                break;
            case 1: // Missing - Red
                holder.relativeLayout.setBackgroundResource(R.drawable.gradient_disaster_mode);
                break;
        }
    }

    class MissingPersonHolder extends RecyclerView.ViewHolder {
        private RelativeLayout relativeLayout;
        private TextView timeText;
        private TextView nameText;
        private TextView phoneText;
        private TextView locationText;
        private TextView sexText;
        private TextView ageText;

        public MissingPersonHolder(@NonNull View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.missing_person_item_layout);
            timeText = itemView.findViewById(R.id.missing_person_item_time);
            nameText = itemView.findViewById(R.id.missing_person_item_username);
            phoneText = itemView.findViewById(R.id.missing_person_item_phone_number);
            locationText = itemView.findViewById(R.id.missing_person_item_location);
            sexText = itemView.findViewById(R.id.missing_person_item_sex);
            ageText = itemView.findViewById(R.id.missing_person_item_age);
        }
    }
}
