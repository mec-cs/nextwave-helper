package meccsb.project.nextwavehelper.missingpersons;

import android.content.Context;
import android.os.Bundle;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.Objects;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.main.TextValidator;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;
import meccsb.project.nextwavehelper.roomdb.viewmodels.MissingPersonViewModel;

public class MissingPersonAddFragment extends Fragment {

    private TextInputLayout textInputName;
    private RadioGroup radioInputSex;
    private TextInputLayout textInputAge;
    private TextInputLayout textInputPhone;
    private TextInputLayout textInputLocation;

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MissingPersonViewModel missingPersonViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        missingPersonViewModel = ViewModelProviders.of(this, viewModelFactory).get(MissingPersonViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_missing_person_add, container, false);
        final ProgressBar progressBar = view.findViewById(R.id.missing_person_form_progress_bar);
        textInputName = view.findViewById(R.id.missing_person_form_username);
        radioInputSex = view.findViewById(R.id.missing_person_form_sex_radio);
        textInputAge = view.findViewById(R.id.missing_person_form_age);
        textInputPhone = view.findViewById(R.id.missing_person_form_phone_number);
        textInputLocation = view.findViewById(R.id.missing_person_form_location);
        Button button = view.findViewById(R.id.missing_person_form_submit);

        Objects.requireNonNull(textInputName.getEditText()).addTextChangedListener(
                new TextValidator(textInputName.getEditText(), "^[A-Za-z \\.]+$", "name"));
        Objects.requireNonNull(textInputPhone.getEditText()).addTextChangedListener(
                new TextValidator(textInputPhone.getEditText(), "^[+]?[0-9]{10,13}$", "phone number"));
        Objects.requireNonNull(textInputAge.getEditText()).addTextChangedListener(
                new TextValidator(textInputAge.getEditText(), "[1-9][0-9]*$", "age"));

        missingPersonViewModel.getStatus().observe(getViewLifecycleOwner(), s -> {
            if (s.equals("SUCCESS")) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getContext(), "Successfully Added!", Toast.LENGTH_LONG).show();
                Objects.requireNonNull(getFragmentManager()).popBackStack();
                missingPersonViewModel.getStatus().setValue("");
                missingPersonViewModel.refreshFromWeb();
            } else if (s.equals("FAILED")) {
                progressBar.setVisibility(View.INVISIBLE);
                Snackbar.make(view, "Failed to add, Please try again later.", Snackbar.LENGTH_LONG).show();
                missingPersonViewModel.getStatus().setValue("");
            }
        });

        button.setOnClickListener(v -> {
            v.performHapticFeedback(HapticFeedbackConstants.KEYBOARD_TAP);
            if (validateEmpty(textInputName) || validateEmpty(textInputAge) || validateEmpty(textInputPhone)
                    || validateEmpty(textInputLocation)) {
                return;
            }
            MissingPerson missingPerson = new MissingPerson();
            missingPerson.setName(getStringFromTextInputLayout(textInputName));
            missingPerson.setAge(Integer.valueOf(getStringFromTextInputLayout(textInputAge)));

            switch (radioInputSex.getCheckedRadioButtonId()) {
                case R.id.missing_person_form_sex_male:
                    missingPerson.setSex('M');
                    break;
                case R.id.missing_person_form_sex_female:
                    missingPerson.setSex('F');
                    break;
            }

            missingPerson.setPhone(getStringFromTextInputLayout(textInputPhone));
            missingPerson.setLocation(getStringFromTextInputLayout(textInputLocation));
            missingPerson.setMissing(1);
            missingPersonViewModel.postToWeb(missingPerson);
            progressBar.setVisibility(View.VISIBLE);
        });
        /* Set title text according to the fragment*/
        Objects.requireNonNull(getActivity()).setTitle("Report Missing Person");
        return view;
    }

    private String getStringFromTextInputLayout(TextInputLayout textInputLayout) {
        return Objects.requireNonNull(textInputLayout.getEditText()).getText().toString().trim();
    }

    private boolean validateEmpty(TextInputLayout textInputLayout) {
        String text = getStringFromTextInputLayout(textInputLayout);
        if (Objects.requireNonNull(textInputLayout.getEditText()).getError() != null) {
            return true;
        }
        if (text.isEmpty()) {
            textInputLayout.setError("Field can't be empty");
            return true;
        } else {
            textInputLayout.setError(null);
            return false;
        }
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
