package meccsb.project.nextwavehelper.missingpersons;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.Fade;
import dagger.android.support.AndroidSupportInjection;
import meccsb.project.nextwavehelper.R;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;
import meccsb.project.nextwavehelper.roomdb.viewmodels.MissingPersonViewModel;

public class MissingPersonsFragment extends Fragment {


    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private MissingPersonViewModel missingPersonViewModel;
    private MissingPersonAdapter adapter = new MissingPersonAdapter();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        missingPersonViewModel = ViewModelProviders.of(this, viewModelFactory).get(MissingPersonViewModel.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        missingPersonViewModel.refreshFromWeb();
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_missing_person, container, false);
        NavigationView navigationView = Objects.requireNonNull(getActivity()).findViewById(R.id.nav_view);

        /* To get the search button in menu*/
        setHasOptionsMenu(true);

        FloatingActionButton fab = view.findViewById(R.id.missing_person_add_fab);
        fab.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                replaceFragments(new MissingPersonAddFragment());
            }
        });

        /* Set title text according to the fragment*/
        getActivity().setTitle(R.string.title_missing_persons);

        final RecyclerView recyclerView = view.findViewById(R.id.missing_person_recycler_view);
        final ProgressBar progressBar = view.findViewById(R.id.missing_person_progressBar);
        final LinearLayout mainContent = view.findViewById(R.id.missing_person_main_content);
        /* Set navigation view item as checked */
        navigationView.setCheckedItem(R.id.nav_missing_persons);

        missingPersonViewModel.getAllMissingPersons().observe(this, new Observer<List<MissingPerson>>() {
            @Override
            public void onChanged(List<MissingPerson> missingPersons) {
                adapter.submitList(missingPersons);
                if(missingPersons.size() > 0) {
                    progressBar.setVisibility(View.GONE);
                    mainContent.setVisibility(View.VISIBLE);
                }
            }
        });

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.search_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(@NonNull Menu menu) {
        SearchView searchView = (SearchView) menu.findItem(R.id.menu_search).getActionView();
        searchView.setQueryHint("Search Missing Persons");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                missingPersonViewModel.setQuery(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                missingPersonViewModel.setQuery(newText);
                return true;
            }
        });
        super.onPrepareOptionsMenu(menu);
    }

    private void replaceFragments(Fragment newFragment) {
        newFragment.setEnterTransition(new Fade(Fade.IN));
        FragmentTransaction fragmentTransaction = Objects.requireNonNull(getFragmentManager()).beginTransaction();
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.replace(R.id.fragment_container, newFragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.configureDagger();
    }

    private void configureDagger() {
        /*
         * Step 2: Remember in our FragmentModule, we
         * defined Fragment injection? So we need
         * to call this method in order to inject the
         * ViewModelFactory into our Fragment
         * */
        AndroidSupportInjection.inject(this);
    }
}
