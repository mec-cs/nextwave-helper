package meccsb.project.nextwavehelper.roomdb.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import meccsb.project.nextwavehelper.roomdb.database.dao.AnnouncementDao;
import meccsb.project.nextwavehelper.roomdb.database.dao.HelpRequestDao;
import meccsb.project.nextwavehelper.roomdb.database.dao.MissingPersonDao;
import meccsb.project.nextwavehelper.roomdb.database.dao.ReliefCampDao;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;

// Add more entities to the curly braces, we put version as 1 because it's a development project.
@Database(entities = {Announcement.class, ReliefCamp.class, HelpRequest.class, MissingPerson.class}, version = 1, exportSchema = false)
public abstract class MyDatabase extends RoomDatabase {
    // Singleton - we cannot create multiple instances of this database.
    private static volatile MyDatabase INSTANCE;

    // DAO for each entities.
    public abstract AnnouncementDao announcementDao();
    public abstract HelpRequestDao helpRequestDao();
    public abstract ReliefCampDao reliefCampDao();

    public abstract MissingPersonDao missingPersonDao();
}