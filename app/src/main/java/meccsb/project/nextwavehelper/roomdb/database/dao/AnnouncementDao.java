package meccsb.project.nextwavehelper.roomdb.database.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface AnnouncementDao {
    @Insert(onConflict = REPLACE)
    void insert(Announcement announcement);

    @Query("DELETE FROM announcements WHERE AnnouncementID = :announcementID;")
    void deleteAnnouncementID(int announcementID);

    @Query("SELECT AnnouncementID FROM announcements;")
    List<Integer> getAllAnnouncementIDs();

    @Query("SELECT * FROM announcements ORDER BY AnnouncementID DESC LIMIT 1")
    LiveData<Announcement> getLatestAnnouncement();

    @Query("SELECT * FROM announcements ORDER BY AnnouncementID DESC")
    LiveData<List<Announcement>> getAllAnnouncements();
}