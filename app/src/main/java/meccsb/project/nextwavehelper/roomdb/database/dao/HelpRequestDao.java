package meccsb.project.nextwavehelper.roomdb.database.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface HelpRequestDao {
    @Insert(onConflict = REPLACE)
    void insert(HelpRequest helpRequest);

    @Query("DELETE FROM helprequests WHERE RequestID = :helpRequestID;")
    void deleteHelpRequestID(int helpRequestID);

    @Query("SELECT RequestID FROM helprequests;")
    List<Integer> getAllHelpRequestIDs();

    @Query("SELECT * FROM helprequests ORDER BY RequestID DESC")
    LiveData<List<HelpRequest>> getAllHelpRequests();

    @Query("SELECT * FROM helprequests WHERE Phone LIKE :phone ORDER BY RequestID DESC;")
    LiveData<List<HelpRequest>> getHelpRequestsByPhone(String phone);

    @Query("SELECT * FROM helprequests WHERE WorkerNumber LIKE :workerNumber ORDER BY RequestID DESC;")
    LiveData<List<HelpRequest>> getHelpRequestsByWorkerNumber(String workerNumber);

    @Query("SELECT * FROM helprequests " +
            "WHERE Name LIKE :query OR Needs LIKE :query OR Location LIKE :query " +
            "ORDER BY Status DESC;")
    LiveData<List<HelpRequest>> searchHelpRequests(String query);
}
