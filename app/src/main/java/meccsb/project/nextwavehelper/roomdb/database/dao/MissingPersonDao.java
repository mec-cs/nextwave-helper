package meccsb.project.nextwavehelper.roomdb.database.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface MissingPersonDao {
    @Insert(onConflict = REPLACE)
    void insert(MissingPerson missingPerson);

    @Delete
    void delete(MissingPerson missingPerson);

    @Query("SELECT PersonID FROM missingpersons;")
    List<Integer> getAllMissingPersonIDs();

    @Query("DELETE FROM missingpersons WHERE PersonID = :missingPersonID;")
    void deleteMissingPersonID(int missingPersonID);

    @Query("SELECT * FROM missingpersons " +
            "WHERE Name LIKE :query OR Phone LIKE :query OR Location LIKE :query "+
            "ORDER BY Name ASC;")
    LiveData<List<MissingPerson>> searchMissingPersons(String query);

    // LiveData is for observing for changes.
    @Query("SELECT * FROM missingpersons ORDER BY missing DESC")
    LiveData<List<MissingPerson>> getAllMissingPersons();
}
