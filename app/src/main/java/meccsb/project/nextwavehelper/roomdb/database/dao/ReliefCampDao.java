package meccsb.project.nextwavehelper.roomdb.database.dao;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;

import static androidx.room.OnConflictStrategy.REPLACE;

@Dao
public interface ReliefCampDao {
    @Insert(onConflict = REPLACE)
    void insert(ReliefCamp reliefCamp);

    @Query("DELETE FROM reliefcamps WHERE CampID = :campID;")
    void deleteReliefCampID(int campID);

    @Query("SELECT CampID FROM reliefcamps;")
    List<Integer> getAllReliefCampIDs();

    @Query("SELECT * FROM reliefcamps ORDER BY Name ASC")
    LiveData<List<ReliefCamp>> getAllReliefCamps();

    @Query("SELECT * FROM reliefcamps " +
            "WHERE Name LIKE :query OR Address LIKE :query " +
            "ORDER BY Name ASC;")
    LiveData<List<ReliefCamp>> searchReliefCamps(String query);

    @Query("SELECT * FROM reliefcamps WHERE CampID = :campID;")
    LiveData<ReliefCamp> getReliefCampById(int campID);
}
