package meccsb.project.nextwavehelper.roomdb.database.entities;

import com.google.gson.annotations.Expose;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "announcements")
public class Announcement {

    @PrimaryKey
    @Expose
    private int AnnouncementID;

    /* We are assuming it to be string, since it is returned from the API */
    @Expose
    private String Time;

    @Expose
    private String Description;

    @Expose
    private int ImportanceLevel;

    public Announcement(){

    }

    public Announcement(int announcementID, String time, String description, int importanceLevel) {
        AnnouncementID = announcementID;
        Time = time;
        Description = description;
        ImportanceLevel = importanceLevel;
    }

    // getters
    public int getAnnouncementID() {
        return AnnouncementID;
    }

    public String getTime() {
        return Time;
    }

    public String getDescription() {
        return Description;
    }

    public int getImportanceLevel() {
        return ImportanceLevel;
    }

    // setters
    public void setAnnouncementID(int announcementID) {
        AnnouncementID = announcementID;
    }

    public void setTime(String time) {
        Time = time;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public void setImportanceLevel(int importanceLevel) {
        ImportanceLevel = importanceLevel;
    }
}
