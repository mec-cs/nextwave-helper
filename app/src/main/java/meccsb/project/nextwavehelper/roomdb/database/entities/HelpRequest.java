package meccsb.project.nextwavehelper.roomdb.database.entities;

import com.google.gson.annotations.Expose;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "helprequests")
public class HelpRequest {
    //Time,Name,Phone,Longitude,Latitude,Location,Needs,Status,WorkerName,WorkerNumber
    @PrimaryKey
    @Expose
    private int RequestID;

    /* We are assuming it to be string, since it is returned from the API */
    @Expose
    private String Time;

    @Expose
    private String Name;

    @Expose
    private String Phone;

    @Expose
    private String Latitude;

    @Expose
    private String Longitude;

    @Expose
    private String Location;

    @Expose
    private String Needs;

    @Expose
    private String Details;

    @Expose
    private int Status;

    @Expose
    private String WorkerName;

    @Expose
    private String WorkerNumber;

    public HelpRequest() {
    }

    public HelpRequest(int requestID, String time, String name, String phone, String latitude, String longitude, String location, String needs, String details,int status, String workerName, String workerNumber) {
        RequestID = requestID;
        Time = time;
        Name = name;
        Phone = phone;
        Longitude = longitude;
        Latitude = latitude;
        Location = location;
        Needs = needs;
        Details = details;
        Status = status;
        WorkerName = workerName;
        WorkerNumber = workerNumber;
    }

    // getters
    public int getRequestID() {
        return RequestID;
    }

    public String getTime() {
        return Time;
    }

    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public String getLongitude() {
        return Longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getLocation() {
        return Location;
    }

    public String getNeeds() {
        return Needs;
    }

    public String getDetails() {
        return Details;
    }

    public int getStatus() {
        return Status;
    }

    public String getWorkerName() {
        return WorkerName;
    }

    public String getWorkerNumber() {
        return WorkerNumber;
    }

    // setters
    public void setRequestID(int requestID) {
        RequestID = requestID;
    }

    public void setTime(String time) {
        Time = time;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public void setNeeds(String needs) {
        Needs = needs;
    }

    public void setDetails(String details) {
        Details = details;
    }

    public void setStatus(int status) {
        Status = status;
    }

    public void setWorkerName(String workerName) {
        WorkerName = workerName;
    }

    public void setWorkerNumber(String workerNumber) {
        WorkerNumber = workerNumber;
    }
}
