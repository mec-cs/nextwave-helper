package meccsb.project.nextwavehelper.roomdb.database.entities;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "missingpersons")
public class MissingPerson {
    //PersonID, Time, Name, Sex, Age, Phone, Location, Missing

    @PrimaryKey(autoGenerate = true)
    private int PersonID;

    private String Time;

    private String Name;

    private char Sex;

    private int Age;

    private String Phone;

    private String Location;

    private int Missing;

    public MissingPerson() {
    }

    public MissingPerson(String time, String name, char sex, int age, String phone, String location, int missing) {
        Time = time;
        Name = name;
        Sex = sex;
        Age = age;
        Phone = phone;
        Location = location;
        Missing = missing;
    }

    public void setPersonID(int personID) {
        PersonID = personID;
    }

    public void setTime(String time) {
        Time = time;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setSex(char sex) {
        Sex = sex;
    }

    public void setAge(int age) {
        Age = age;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setLocation(String location) {
        Location = location;
    }

    public void setMissing(int missing) {
        Missing = missing;
    }

    public int getPersonID() {
        return PersonID;
    }

    public String getTime() {
        return Time;
    }

    public String getName() {
        return Name;
    }

    public char getSex() {
        return Sex;
    }

    public int getAge() {
        return Age;
    }

    public String getPhone() {
        return Phone;
    }

    public String getLocation() {
        return Location;
    }

    public int getMissing() {
        return Missing;
    }
}
