package meccsb.project.nextwavehelper.roomdb.database.entities;

import com.google.gson.annotations.Expose;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "reliefcamps")
public class ReliefCamp {
    @PrimaryKey
    @Expose
    private int CampID;

    @Expose
    private String Name;

    @Expose
    private String Phone;

    @Expose
    private String Longitude;

    @Expose
    private String Latitude;

    @Expose
    private String Address;

    @Ignore
    private Float DistanceToCurrLoc = -1f;

    // Constructors
    public ReliefCamp() {
    }

    public ReliefCamp(int campID, String name, String phone, String longitude, String latitude, String address) {
        CampID = campID;
        Name = name;
        Phone = phone;
        Longitude = longitude;
        Latitude = latitude;
        Address = address;
    }

    // Getters
    public int getCampID() {
        return CampID;
    }

    public String getName() {
        return Name;
    }

    public String getPhone() {
        return Phone;
    }

    public String getLongitude() {
        return Longitude;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getAddress() {
        return Address;
    }

    public Float getDistanceToCurrLoc() {
        return DistanceToCurrLoc;
    }
// Setters

    public void setCampID(int campID) {
        CampID = campID;
    }

    public void setName(String name) {
        Name = name;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public void setDistanceToCurrLoc(Float distanceToCurrLoc) {
        DistanceToCurrLoc = distanceToCurrLoc;
    }
}
