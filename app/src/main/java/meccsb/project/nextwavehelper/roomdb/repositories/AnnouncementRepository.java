package meccsb.project.nextwavehelper.roomdb.repositories;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.lifecycle.LiveData;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.api.AnnouncementWebservice;
import meccsb.project.nextwavehelper.roomdb.database.dao.AnnouncementDao;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class AnnouncementRepository {
    private final Executor executor;
    private final AnnouncementWebservice webservice;
    private final AnnouncementDao announcementDao;
    private final LiveData<List<Announcement>> allAnnouncements;
    private List<Integer> allLocalIDs;
    private final LiveData<Announcement> latestAnnouncement;

    @Inject
    public AnnouncementRepository(AnnouncementWebservice webservice, AnnouncementDao announcementDao, Executor executor){
        this.announcementDao = announcementDao;
        this.webservice = webservice;
        this.executor = executor;
        allAnnouncements = announcementDao.getAllAnnouncements();
        latestAnnouncement = announcementDao.getLatestAnnouncement();
    }

    public LiveData<List<Announcement>> getAllAnnouncements() {
        return allAnnouncements;
    }

    public LiveData<Announcement> getLatestAnnouncement(){
        return latestAnnouncement;
    }

    public void refreshFromWeb(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                allLocalIDs = announcementDao.getAllAnnouncementIDs();
                final List<Integer> staleLocalIDs = new ArrayList<>(allLocalIDs.size());
                final List<Integer> allWebIDs = new ArrayList<>();

                webservice.getAnnouncements().enqueue(new Callback<List<Announcement>>() {
                    @Override
                    public void onResponse(Call<List<Announcement>> call, Response<List<Announcement>> response) {
                        Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_SHORT).show();
                        List<Announcement> announcements = response.body();
                        assert announcements != null;
                        for(Announcement announcement: announcements) {
                            allWebIDs.add(announcement.getAnnouncementID());
                            insert(announcement);
                        }

                        // Set difference
                        staleLocalIDs.addAll(allLocalIDs);
                        staleLocalIDs.removeAll(allWebIDs);

                        //Remove old entries
                        for(Integer id:staleLocalIDs){
                            deleteID(id);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Announcement>> call, Throwable t) {
                        Toast.makeText(App.context, "Data couldn't be refreshed from network !", Toast.LENGTH_SHORT).show();
                        Log.e("TAG", "DATA COULDN'T BE REFRESHED FROM NETWORK, ERROR: " + t.getMessage());
                    }
                });
            }
        });
    }

    private void insert(final Announcement announcement){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                announcementDao.insert(announcement);
            }
        });
    }

    private void deleteID(final int Id){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                announcementDao.deleteAnnouncementID(Id);
            }
        });
    }
}
