package meccsb.project.nextwavehelper.roomdb.repositories;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.api.HelpRequestWebservice;
import meccsb.project.nextwavehelper.roomdb.database.dao.HelpRequestDao;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HelpRequestRepository {
    private final Executor executor;
    private final HelpRequestWebservice webservice;
    private final HelpRequestDao helpRequestDao;
    private final LiveData<List<HelpRequest>> allHelpRequests;
    static private MutableLiveData<String> status = new MutableLiveData<>();
    private List<Integer> allLocalIDs;

    @Inject
    public HelpRequestRepository(HelpRequestWebservice webservice, HelpRequestDao helpRequestDao, Executor executor){
        this.helpRequestDao = helpRequestDao;
        this.webservice = webservice;
        this.executor = executor;
        status.setValue("");
        allHelpRequests = helpRequestDao.getAllHelpRequests();
    }

    public LiveData<List<HelpRequest>> getAllHelpRequests(){
        return  allHelpRequests;
    }

    public LiveData<List<HelpRequest>> getHelpRequestsByPhone(String phone) {
        return helpRequestDao.getHelpRequestsByPhone(phone);
    }

    public LiveData<List<HelpRequest>> getHelpRequestsByWorkerNumber(String workerNumber) {
        return helpRequestDao.getHelpRequestsByWorkerNumber(workerNumber);
    }

    public LiveData<List<HelpRequest>> searchHelpRequests(String query) {
        return helpRequestDao.searchHelpRequests("%" + query + "%");
    }

    public void refreshFromWeb(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                allLocalIDs = helpRequestDao.getAllHelpRequestIDs();
                final List<Integer> staleLocalIDs = new ArrayList<>(allLocalIDs.size());
                final List<Integer> allWebIDs = new ArrayList<>();

                webservice.getHelpRequests().enqueue(new Callback<List<HelpRequest>>() {
                    @Override
                    public void onResponse(Call<List<HelpRequest>> call, Response<List<HelpRequest>> response) {
                        Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_SHORT).show();
                        List<HelpRequest> helpRequests = response.body();
                        assert helpRequests != null;
                        for(HelpRequest helpRequest: helpRequests) {
                            allWebIDs.add(helpRequest.getRequestID());
                            insert(helpRequest);
                        }

                        // Set difference
                        staleLocalIDs.addAll(allLocalIDs);
                        staleLocalIDs.removeAll(allWebIDs);

                        //Remove old entries
                        for(Integer id:staleLocalIDs){
                            deleteID(id);
                        }
                    }
                    @Override
                    public void onFailure(Call<List<HelpRequest>> call, Throwable t) {
                        Toast.makeText(App.context, "Data couldn't be refreshed from network !", Toast.LENGTH_SHORT).show();
                        Log.e("TAG", "DATA COULDN'T BE REFRESHED FROM NETWORK, ERROR: " + t.getMessage());
                    }
                });
            }
        });
    }

    public void postToWeb(HelpRequest helpRequest) {
        webservice.addHelpRequest(helpRequest).enqueue(new Callback<HelpRequest>() {
            @Override
            public void onResponse(Call<HelpRequest> call, Response<HelpRequest> response) {
                if (response.isSuccessful()) {
                    status.setValue("SUCCESS");
                }
                else {
                    Log.d("this", ""+ response.code()+response.body());
                }
            }
            @Override
            public void onFailure(Call<HelpRequest> call, Throwable t) {
                status.setValue("FAILED");
                Log.e("TAG", "DATA COULDN'T BE ADDED, ERROR: " + t.getMessage());
            }
        });
    }

    private void insert(final HelpRequest helpRequest){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                helpRequestDao.insert(helpRequest);
            }
        });
    }

    private void deleteID(final int Id){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                helpRequestDao.deleteHelpRequestID(Id);
            }
        });
    }

    public MutableLiveData<String> getStatus() {
        return status;
    }

}
