package meccsb.project.nextwavehelper.roomdb.repositories;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.api.MissingPersonWebservice;
import meccsb.project.nextwavehelper.roomdb.database.dao.MissingPersonDao;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class MissingPersonRepository {
    private Executor executor;
    private final MissingPersonWebservice webservice;
    private MissingPersonDao missingPersonDao;
    private LiveData<List<MissingPerson>> allMissingPersons;
    private List<Integer> allLocalIDs;
    static private MutableLiveData<String> status = new MutableLiveData<>();

    @Inject
    public MissingPersonRepository(MissingPersonWebservice webservice, MissingPersonDao missingPersonDao, Executor executor) {
        this.missingPersonDao = missingPersonDao;
        this.webservice = webservice;
        this.executor = executor;
        status.setValue("");
        allMissingPersons = missingPersonDao.getAllMissingPersons();
    }

    public LiveData<List<MissingPerson>> searchMissingPersons(String query){
        return missingPersonDao.searchMissingPersons("%" + query + "%");
    }

    public LiveData<List<MissingPerson>> getAllMissingPersons() {
        return allMissingPersons;
    }

    public void postToWeb(MissingPerson missingPerson) {
        webservice.addMissingPerson(missingPerson).enqueue(new Callback<MissingPerson>() {
            @Override
            public void onResponse(Call<MissingPerson> call, Response<MissingPerson> response) {
                if (response.isSuccessful()) {
                    status.setValue("SUCCESS");
                }
            }
            @Override
            public void onFailure(Call<MissingPerson> call, Throwable t) {
                status.setValue("FAILED");
                Log.e("TAG", "DATA COULDN'T BE ADDED, ERROR: " + t.getMessage());
            }
        });
    }

    public void refreshFromWeb() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                allLocalIDs = missingPersonDao.getAllMissingPersonIDs();
                final List<Integer> staleLocalIDs = new ArrayList<>(allLocalIDs.size());
                final List<Integer> allWebIDs = new ArrayList<>();

                webservice.getMissingPersons().enqueue(new Callback<List<MissingPerson>>() {
                    @Override
                    public void onResponse(Call<List<MissingPerson>> call, Response<List<MissingPerson>> response) {
                        Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_SHORT).show();
                        List<MissingPerson> missingPersons = response.body();
                        assert missingPersons != null;
                        for (MissingPerson missingPerson : missingPersons) {
                            allWebIDs.add(missingPerson.getPersonID());
                            insert(missingPerson);
                        }

                        // Set difference
                        staleLocalIDs.addAll(allLocalIDs);
                        staleLocalIDs.removeAll(allWebIDs);

                        //Remove old entries
                        for (Integer id : staleLocalIDs) {
                            deleteID(id);
                        }
                    }

                    @Override
                    public void onFailure(Call<List<MissingPerson>> call, Throwable t) {
                        Toast.makeText(App.context, "Data couldn't be refreshed from network !", Toast.LENGTH_SHORT).show();
                        Log.e("TAG", "DATA COULDN'T BE REFRESHED FROM NETWORK, ERROR: " + t.getMessage());
                    }
                });
            }
        });
    }

    private void insert(final MissingPerson missingPerson) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                missingPersonDao.insert(missingPerson);
            }
        });
    }

    private void deleteID(final int Id) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                missingPersonDao.deleteMissingPersonID(Id);
            }
        });
    }

    public MutableLiveData<String> getStatus() {
        return status;
    }
}
