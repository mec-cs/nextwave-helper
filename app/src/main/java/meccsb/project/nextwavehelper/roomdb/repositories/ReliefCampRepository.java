package meccsb.project.nextwavehelper.roomdb.repositories;

import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import meccsb.project.nextwavehelper.App;
import meccsb.project.nextwavehelper.api.ReliefCampWebservice;
import meccsb.project.nextwavehelper.roomdb.database.dao.ReliefCampDao;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReliefCampRepository {
    private final Executor executor;
    private final ReliefCampWebservice webservice;
    private final ReliefCampDao reliefCampDao;
    private final LiveData<List<ReliefCamp>> allReliefCamps;
    private List<Integer> allLocalIDs;

    @Inject
    public ReliefCampRepository(ReliefCampWebservice webservice, ReliefCampDao reliefCampDao, Executor executor){
        this.reliefCampDao = reliefCampDao;
        this.webservice = webservice;
        this.executor = executor;
        allReliefCamps = reliefCampDao.getAllReliefCamps();
    }

    public LiveData<List<ReliefCamp>> getAllReliefCamps() {
        return allReliefCamps;
    }

    public LiveData<ReliefCamp> getReliefCampById(int id) {
        return reliefCampDao.getReliefCampById(id);
    }

    public LiveData<List<ReliefCamp>> searchReliefCamps(String query){
        return reliefCampDao.searchReliefCamps("%" + query + "%");
    }

    public void refreshFromWeb(){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                allLocalIDs = reliefCampDao.getAllReliefCampIDs();
                final List<Integer> staleLocalIDs = new ArrayList<>(allLocalIDs.size());
                final List<Integer> allWebIDs = new ArrayList<>();

                webservice.getReliefCamps().enqueue(new Callback<List<ReliefCamp>>() {
                    @Override
                    public void onResponse(Call<List<ReliefCamp>> call, Response<List<ReliefCamp>> response) {
                        Toast.makeText(App.context, "Data refreshed from network !", Toast.LENGTH_SHORT).show();
                        List<ReliefCamp> reliefCamps = response.body();
                        if(reliefCamps != null) {
                            for (ReliefCamp reliefCamp : reliefCamps) {
                                allWebIDs.add(reliefCamp.getCampID());
                                insert(reliefCamp);
                            }
                        }
                        // Set difference
                        staleLocalIDs.addAll(allLocalIDs);
                        staleLocalIDs.removeAll(allWebIDs);

                        //Remove old entries
                        for(Integer id:staleLocalIDs){
                            deleteID(id);
                        }
                    }
                    @Override
                    public void onFailure(Call<List<ReliefCamp>> call, Throwable t) {
                        Toast.makeText(App.context, "Data couldn't be refreshed from network !", Toast.LENGTH_SHORT).show();
                        Log.e("TAG", "DATA COULDN'T BE REFRESHED FROM NETWORK, ERROR: " + t.getMessage());
                    }
                });
            }
        });
    }

    private void insert(final ReliefCamp reliefCamp){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                reliefCampDao.insert(reliefCamp);
            }
        });
    }

    private void deleteID(final int Id){
        executor.execute(new Runnable() {
            @Override
            public void run() {
                reliefCampDao.deleteReliefCampID(Id);
            }
        });
    }
}
