package meccsb.project.nextwavehelper.roomdb.viewmodels;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import meccsb.project.nextwavehelper.roomdb.database.entities.Announcement;
import meccsb.project.nextwavehelper.roomdb.repositories.AnnouncementRepository;

public class AnnouncementViewModel extends ViewModel {
    private final AnnouncementRepository repository;
    private final LiveData<List<Announcement>> allAnnouncements;
    private final LiveData<Announcement> latestAnnouncement;

    @Inject
    public AnnouncementViewModel(AnnouncementRepository repository){
        this.repository = repository;
        allAnnouncements = repository.getAllAnnouncements();
        latestAnnouncement = repository.getLatestAnnouncement();
    }

    public void refreshFromWeb(){
        repository.refreshFromWeb();
    }

    public LiveData<List<Announcement>> getAllAnnouncements() {
        return allAnnouncements;
    }

    public LiveData<Announcement> getLatestAnnouncement() {
        return latestAnnouncement;
    }
}
