package meccsb.project.nextwavehelper.roomdb.viewmodels;

import android.location.Location;

import java.util.List;

import javax.inject.Inject;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import meccsb.project.nextwavehelper.roomdb.database.entities.HelpRequest;
import meccsb.project.nextwavehelper.roomdb.repositories.HelpRequestRepository;

public class HelpRequestViewModel extends ViewModel{
    private final HelpRequestRepository repository;
    private LiveData<List<HelpRequest>> allHelpRequests;
    private final MutableLiveData<String> query;
    private static MutableLiveData<Location> currLoc = new MutableLiveData<>();

    @Inject
    public HelpRequestViewModel(final HelpRequestRepository repository) {
        this.repository = repository;
        this.query = new MutableLiveData<>();
        this.query.setValue("");
        allHelpRequests = Transformations.switchMap(query, new Function<String, LiveData<List<HelpRequest>>>() {
            @Override
            public LiveData<List<HelpRequest>> apply(String input) {
                if (input.isEmpty()) {
                    return repository.getAllHelpRequests();
                }
                return repository.searchHelpRequests(input);
            }
        });

    }
    public void refreshFromWeb(){
        repository.refreshFromWeb();
    }

    public LiveData<List<HelpRequest>> getAllHelpRequests() {
        return allHelpRequests;
    }


    public LiveData<List<HelpRequest>> getRegisteredRequests(String phone) {
        return repository.getHelpRequestsByPhone(phone);
    }

    public LiveData<List<HelpRequest>> getAssignedHelpRequests(String workerNumber) {
        return repository.getHelpRequestsByWorkerNumber(workerNumber);
    }

    public void setQuery(String query) {
        this.query.setValue(query);
    }

    public void postToWeb(HelpRequest helpRequest) {repository.postToWeb(helpRequest);}

    public MutableLiveData<Location> getCurrLoc() {
        return this.currLoc;
    }

    public MutableLiveData<String> getStatus() {
        return repository.getStatus();
    }
}
