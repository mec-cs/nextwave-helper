package meccsb.project.nextwavehelper.roomdb.viewmodels;

import java.util.List;

import javax.inject.Inject;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import meccsb.project.nextwavehelper.roomdb.database.entities.MissingPerson;
import meccsb.project.nextwavehelper.roomdb.repositories.MissingPersonRepository;

public class MissingPersonViewModel extends ViewModel {
    private MissingPersonRepository repository;
    private MutableLiveData<String> query;
    private LiveData<List<MissingPerson>> allMissingPersons;

    @Inject
    public MissingPersonViewModel(final MissingPersonRepository repository) {
        this.repository = repository;
        this.query = new MutableLiveData<>();
        this.query.setValue("");
        allMissingPersons = Transformations.switchMap(query, new Function<String, LiveData<List<MissingPerson>>>() {
            @Override
            public LiveData<List<MissingPerson>> apply(String input) {
                if(input.isEmpty()){
                    return repository.getAllMissingPersons();
                }
                return repository.searchMissingPersons(input);
            }
        });
    }

    public void refreshFromWeb() {
        repository.refreshFromWeb();
    }

    public void postToWeb(MissingPerson missingPerson) {repository.postToWeb(missingPerson);}

    public void setQuery(String query) {
        this.query.setValue(query);
    }

    public LiveData<List<MissingPerson>> getAllMissingPersons() {
        return allMissingPersons;
    }

    public MutableLiveData<String> getStatus() {
        return repository.getStatus();
    }
}
