package meccsb.project.nextwavehelper.roomdb.viewmodels;

import android.location.Location;

import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import meccsb.project.nextwavehelper.roomdb.database.entities.ReliefCamp;
import meccsb.project.nextwavehelper.roomdb.repositories.ReliefCampRepository;

public class ReliefCampViewModel extends ViewModel {
    private final ReliefCampRepository repository;
    private final LiveData<List<ReliefCamp>> allReliefCamps;
    private final MutableLiveData<String> query;
    private final MutableLiveData<Location> currentLocation;

    @Inject
    public ReliefCampViewModel(final ReliefCampRepository repository){
        this.repository = repository;
        this.query = new MutableLiveData<>();
        this.currentLocation = new MutableLiveData<>();

        this.query.setValue("");
        final LiveData<List<ReliefCamp>> filteredReliefCamp = Transformations.switchMap(query, new Function<String, LiveData<List<ReliefCamp>>>() {
            @Override
            public LiveData<List<ReliefCamp>> apply(String input) {
                if(input.isEmpty()){
                    return repository.getAllReliefCamps();
                }
                return repository.searchReliefCamps(input);
            }
        });

        allReliefCamps = Transformations.switchMap(currentLocation, new Function<Location, LiveData<List<ReliefCamp>>>() {
            @Override
            public LiveData<List<ReliefCamp>> apply(final Location currLoc) {
                // If default value then do nothing.
                if (currLoc.getLongitude() == 0.0d)
                    return filteredReliefCamp;
                else
                    return Transformations.map(filteredReliefCamp, new Function<List<ReliefCamp>, List<ReliefCamp>>() {
                        @Override
                        public List<ReliefCamp> apply(List<ReliefCamp> reliefCamps) {
                            // Calculate the distance and store in variable to display later.
                            for (ReliefCamp camp : reliefCamps) {
                                Location loc = new Location("");
                                loc.setLongitude(Double.valueOf(camp.getLongitude()));
                                loc.setLatitude(Double.valueOf(camp.getLatitude()));
                                float distance = currLoc.distanceTo(loc);
                                camp.setDistanceToCurrLoc(distance / 1000);
                            }
                            // Arrange the relief-camps in the order of the distance.
                            reliefCamps.sort(new Comparator<ReliefCamp>() {
                                @Override
                                public int compare(ReliefCamp o1, ReliefCamp o2) {
                                    return o1.getDistanceToCurrLoc().compareTo(o2.getDistanceToCurrLoc());
                                }
                            });
                            return reliefCamps;
                        }
                    });
            }
        });
    }

    public void refreshFromWeb(){
        repository.refreshFromWeb();
    }

    public LiveData<List<ReliefCamp>> getAllReliefCamps() {
        return allReliefCamps;
    }

    public LiveData<ReliefCamp> getReliefCampById(int id) {
        return repository.getReliefCampById(id);
    }
    public void setQuery(String query) {
        this.query.setValue(query);
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation.setValue(currentLocation);
    }
}
